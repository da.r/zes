FROM openjdk:11

COPY jar jar

RUN apt-get update && apt-get install -y \
libxrender1 \
libxtst6 \
libxi6 \
libgtk3.0 # package needed for another look and feel can be used here


WORKDIR "jar"

# Look and feel of the GUI can be manipulated with the second argument
ENTRYPOINT ["java", "-jar", "-Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel", "ZES.jar"]
                                
                                
