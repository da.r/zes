package model;

import java.time.LocalDate;

/**
 * Represents an entry with an absence reason.
 */
public class AbsenceEntry extends Entry {
	String absenceReason;
	
	/**
	 * Constructor for entry with an absence reason.
	 * 
	 * @param date date on which the user was absent
	 * @param absenceReason reason for absence
	 */
	public AbsenceEntry(LocalDate date, String absenceReason) {
		super(date);
		this.absenceReason = absenceReason;
	}

	/**
	 * @return the absenceReason
	 */
	public String getAbsenceReason() {
		return absenceReason;
	}

	/**
	 * @param absenceReason the absenceReason to set
	 */
	public void setAbsenceReason(String absenceReason) {
		this.absenceReason = absenceReason;
	}
	
	
}
