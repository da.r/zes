package model;

/**
 * Class that represents the roles in which the users can be.
 * Role specifies which functionalities of the system are accessible to the user.
 */
public class Role {
	
	private String name;
	
	public boolean startEndTimeDocumentation;

	public boolean absenceReasonInput;
	public boolean accessData;
	
	public boolean userAdministration;
	public boolean systemAdjustment;
	public boolean permissionsAdministration;
	
	/**
	 * Constructor that creates a new role with the given name. All access rights
	 * are set to false.
	 * @param name
	 */
	public Role(String name) {
		this.name = name;
		absenceReasonInput = false;
		startEndTimeDocumentation = false;
		accessData = false;
		userAdministration = false;
		systemAdjustment = false;
		permissionsAdministration = false;
	}
	
	/**
	 * Constructor that creates a new role with the given name and access rights.
	 * 
	 * @param name
	 * @param fE
	 * @param kommGehZeit
	 * @param acData
	 * @param uA
	 * @param sA
	 * @param pA
	 */
	public Role(String name, boolean kommGehZeit, boolean fE, boolean acData, 
				 boolean uA, boolean sA, boolean pA)
	{
		this.name = name;
		startEndTimeDocumentation = kommGehZeit;
		absenceReasonInput = fE;
		accessData = acData;
		userAdministration = uA;
		systemAdjustment = sA;
		permissionsAdministration = pA;
	}
	
	public Role(String name, boolean accessRights[]) {
		this.name = name;
		startEndTimeDocumentation = accessRights[0];
		absenceReasonInput = accessRights[1];
		accessData = accessRights[2];
		userAdministration = accessRights[3];
		systemAdjustment = accessRights[4];
		permissionsAdministration = accessRights[5];
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	
}
