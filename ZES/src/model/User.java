package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.TreeMap;


/**
 * Class that represents a user.
 */
public class User {
	
	private String name;
	private String password;
	private Role role;
	private int hoursPerWeek;
	
	// Using an ordered map instead of set to get around the fact that a 
	// set does not contain get method.
	Map<LocalDate, Entry> entries;
	
	private boolean atWork = false;
	
	private double sumOfWorkTime;
	// Can be positive or negative depending on that whether the worker worked
	// more or less than hers/his hoursPerWeek
	private double workTimeStatus;
	
	private int daysWorked;
	
	/**
	 * Initializes the user object with the given properties.
	 * 
	 * @param name
	 * @param password
	 * @param role
	 * @param hoursPerWeek
	 */
	public User(String name, String password, Role role, int hoursPerWeek) {
		this.name = name;
		this.password = password;
		this.role = role;
		this.hoursPerWeek = hoursPerWeek;
		
		sumOfWorkTime = 0;
		workTimeStatus = 0;
		daysWorked = 0;

		entries = new TreeMap<>();
	}
	
	/**
	 * Initializes the user object with the given properties.
	 * 
	 * @param name
	 * @param password
	 * @param role
	 * @param hoursPerWeek
	 * @param sumOfWorkTime
	 * @param workTimeStatus
	 * @param daysWorked
	 */
	public User(String name, String password, Role role, int hoursPerWeek, double sumOfWorkTime,
			double workTimeStatus, int daysWorked) 
	{
		this.name = name;
		this.password = password;
		this.role = role;
		this.hoursPerWeek = hoursPerWeek;
		this.sumOfWorkTime = sumOfWorkTime;
		this.workTimeStatus = workTimeStatus;
		this.daysWorked = daysWorked;

		entries = new TreeMap<>();
	}
	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param rolle the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}



	/**
	 * @return the entries
	 */
	public Map<LocalDate, Entry> getEntries() {
		return entries;
	}


	/**
	 * @param entries the entries to set
	 */
	public void setEntries(Map<LocalDate, Entry> entries) {
		this.entries = entries;
	}
	
	/**
	 * Returns an entry on the given date.
	 * 
	 * @param date 
	 * @return entry that corresponds to the date or null if there is no entry
	 * on the given date
	 */
	public Entry getEntryOnDate(LocalDate date) {
		return entries.get(date);
	}
	
	/**
	 * Checks whether a note should be set to true. If it should be, then it
	 * sets it to true.
	 * 
	 * @param newEntry entry that is added to the system
	 */
	private void checkSetNote(Entry newEntry) {
		if (newEntry instanceof TimeEntry) {
			TimeEntry newTimeEntry = (TimeEntry) newEntry;
			if (newTimeEntry.getStartTime() != null) {
				if (newTimeEntry.getStartTime().isAfter(Model.startFlexibility[1])) {
					newTimeEntry.setNote(true);
				}
			} 
			if (newTimeEntry.getEndTime() != null) {
				if (newTimeEntry.getEndTime().isBefore(Model.endFlexibility[0])) {
					newTimeEntry.setNote(true);
				}
			}
		}
	}
	
	/**
	 * Helper function, calculates worked hours in an entry.
	 *  
	 * @param entry
	 * @return hours worked, if entry is not a time entry it returns -2, 
	 * if an entry is a time entry without end time, it returns -1
	 */
	private double calculateWorkedHours(Entry entry) {
		long secondsWorked = 0;
		if (entry instanceof TimeEntry) {
			TimeEntry timeEntry = (TimeEntry) entry;
			
			// If entry doesn't contain end time i.e. if the user is still 
			// at work
			if (timeEntry.getEndTime() == null) {
				return -1;
			}
			
			long startPauseStart = ChronoUnit.SECONDS.between(timeEntry.getStartTime(), 
					Model.pauseStartEnd[0]); 
			long endPauseStart = ChronoUnit.SECONDS.between(timeEntry.getEndTime(), 
					Model.pauseStartEnd[0]);
			long startPauseEnd = ChronoUnit.SECONDS.between(timeEntry.getStartTime(), 
					Model.pauseStartEnd[1]); 
			long endPauseEnd = ChronoUnit.SECONDS.between(timeEntry.getEndTime(), 
					Model.pauseStartEnd[1]);
			// If both, started and ended, before the pause or after the pause
			if ((startPauseStart > 0 && endPauseStart > 0) 
					|| (startPauseEnd < 0 && endPauseEnd < 0)) {
				secondsWorked  = ChronoUnit.SECONDS.between(timeEntry.getStartTime(), 
						timeEntry.getEndTime()); 
			// else if started before the pause and ended during the pause
			} else if (startPauseStart > 0 && endPauseStart < 0 && endPauseEnd > 0) {
				secondsWorked = startPauseStart;
			// else if started before the pause and ended after the pause
			} else if (startPauseStart > 0 && endPauseEnd < 0 ) {
				secondsWorked = startPauseStart + (-endPauseEnd);
			// else if started during the pause and ended during the pause
			} else if (startPauseStart < 0 && startPauseEnd > 0 && endPauseEnd > 0) {
				secondsWorked = 0;
			// else if started during the pause and ended after the pause
			} else if (startPauseStart < 0 && startPauseEnd > 0 && endPauseEnd < 0) {
				secondsWorked = -endPauseEnd;
			}
			return (double)secondsWorked / 3600.0;
		} else {
			return -2.0;
		}
	}
	
	/**
	 * Adds a given entry if an entry on the same date does not already exist.
	 * It also sets the note of the entry if it is outside the flexibility limits.
	 * It updates the sum of working time and work time status as well.
	 * 
	 * @param newEntry entry to be added
	 */
	public void addEntry(Entry newEntry) {
		if (entries.putIfAbsent(newEntry.getDate(), newEntry) == null) {
			double workedHours;
			if ((workedHours = calculateWorkedHours(newEntry)) >= 0) {
				setSumOfWorkTime(getSumOfWorkTime() + workedHours);
				setWorkTimeStatus(getWorkTimeStatus() + (workedHours - (hoursPerWeek / 5.0)));
				setDaysWorked(getDaysWorked() + 1);
			}
		}
		checkSetNote(newEntry);
	}
	
	/**
	 * Adds a given entry. If an entry with the same date already exists 
	 * it is overwritten.
	 * It also sets the note of the entry if it is outside the flexibility limits.
	 * It updates the sum of working time and work time status as well.
	 * 
	 * @param newEntry entry to be added
	 */
	public void addEntryOverwrite(Entry newEntry) {
		Entry oldEntry = entries.get(newEntry.getDate());
		double oldWorkedHours = calculateWorkedHours(oldEntry);
		double newWorkedHours = calculateWorkedHours(newEntry);
		if (oldWorkedHours < 0) {
			if (newWorkedHours >= 0) {
				setSumOfWorkTime(getSumOfWorkTime() + newWorkedHours);
				setWorkTimeStatus(getWorkTimeStatus() + (newWorkedHours - (hoursPerWeek / 5.0)));
				setDaysWorked(getDaysWorked() + 1);
			}
		} else {
			if (newWorkedHours < 0) {
				setSumOfWorkTime(getSumOfWorkTime() - oldWorkedHours);
				setWorkTimeStatus(getWorkTimeStatus() - (oldWorkedHours - (hoursPerWeek / 5.0)));
				setDaysWorked(getDaysWorked() - 1);
			} else {
				setSumOfWorkTime(getSumOfWorkTime() + (newWorkedHours - oldWorkedHours));
				setWorkTimeStatus(getWorkTimeStatus() + (newWorkedHours - (hoursPerWeek / 5.0)));
			}
		}
		entries.put(newEntry.getDate(), newEntry);
		checkSetNote(newEntry);
	}
	
	/**
	 * Adds a given entry to the system, but does not update statistics.
	 * If an entry on the same date exists, it is overwritten.
	 * @param newEntry
	 */
	public void addEntryNoStats(Entry newEntry) {
		entries.put(newEntry.getDate(), newEntry);
		checkSetNote(newEntry);
	}
	
	/**
	 * Removes an entry on the given date
	 * Updates the sum of working time and work time status as well.
	 * @param date
	 */
	public void removeEntryOnDate(LocalDate date) {
		Entry oldEntry = entries.remove(date);
		if (oldEntry != null) {
			double oldWorkedHours = calculateWorkedHours(oldEntry);
			if (oldWorkedHours > 0) {
				setSumOfWorkTime(getSumOfWorkTime() - oldWorkedHours);
				setWorkTimeStatus(getWorkTimeStatus() - (oldWorkedHours - (hoursPerWeek / 5.0)));
				setDaysWorked(getDaysWorked() - 1);
			}
		}
	}
	
	/**
	 * @return the atWork
	 */
	public boolean isAtWork() {
		return atWork;
	}

	/**
	 * @param atWork the atWork to set
	 */
	public void setAtWork(boolean atWork) {
		this.atWork = atWork;
	}

	/**
	 * @return the hoursPerWeek
	 */
	public int getHoursPerWeek() {
		return hoursPerWeek;
	}


	/**
	 * @return the sumOfWorkTime
	 */
	public double getSumOfWorkTime() {
		return sumOfWorkTime;
	}


	/**
	 * @param sumOfWorkTime the sumOfWorkTime to set
	 */
	public void setSumOfWorkTime(double sumOfWorkTime) {
		this.sumOfWorkTime = sumOfWorkTime;
	}


	/**
	 * @return the workTimeStatus
	 */
	public double getWorkTimeStatus() {
		return workTimeStatus;
	}


	/**
	 * @param workTimeStatus the workTimeStatus to set
	 */
	public void setWorkTimeStatus(double workTimeStatus) {
		this.workTimeStatus = workTimeStatus;
	}


	/**
	 * @return the daysWorked
	 */
	public int getDaysWorked() {
		return daysWorked;
	}


	/**
	 * @param daysWorked the daysWorked to set
	 */
	public void setDaysWorked(int daysWorked) {
		this.daysWorked = daysWorked;
	}
	
	/**
	 * Calculates statistics (i.e. sumOfWorkTime, workTimeStatus 
	 * and average time worked) in the given interval.
	 *  
	 * @param start start of the interval
	 * @param end end of the interval
	 * @return array of doubles that contains (1) sumOfWorkTime, 
	 * (2) average time worked per day and (3) workTimeStatus
	 */
	public double[] calculateStatistics(LocalDate start, LocalDate end) {
		double stats[] = new double[3];
		LocalDate currentDate = start;
		
		double sum = 0;
		double days = 0;
		double status = 0;
		while (!currentDate.isAfter(end)) {
			if (this.getEntryOnDate(currentDate) != null) {
				double workedHours = calculateWorkedHours(this.getEntryOnDate(currentDate)); 
				if (workedHours >= 0) {
					sum += workedHours;
					days += 1;
					status += workedHours - (hoursPerWeek / 5.0);
				}
			}
			currentDate = currentDate.plusDays(1);
		}
		stats[0] = sum;
		stats[1] =  sum / days;
		stats[2] = status;
		
		return stats;
	}

}
