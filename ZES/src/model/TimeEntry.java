package model;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Represents an entry with start and end time.
 */
public class TimeEntry extends Entry {
	
	private LocalTime startTime;
	private LocalTime endTime;
	
	/**
	 * Constructor for the entry with start and end time.
	 * 
	 * @param date date on which the user worked
	 * @param startTime time when the user came to work
	 * @param endTime time when user went from work
	 */
	public TimeEntry(LocalDate date, LocalTime startTime, LocalTime endTime) {
		super(date);
		this.startTime = startTime;
		this.endTime = endTime;
	}

	/**
	 * @return the startTime
	 */
	public LocalTime getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public LocalTime getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}
	
}
