package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Vector;

/**
 * Contains various helper functions and constructs.
 */
public class Utilities {
	
	// Not so secret key
	public static String encryptionKey = "1234567890123456";
	
	public static int currentVec = -1;
	public static int currentIdxInVec = -1;
	
	public static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	public static DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	
	public static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
	public static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public static Vector<Vector<String>> usersTable = new Vector<>();
	public static Vector<Vector<Object>> rolesTable = new Vector<>();
	
	public static boolean defaultDataLoaded = false;
	
	
	/**
	 *  Loads default data.
	 *  Extension that should make the application more robust.
	 */
	public static void loadDefaultData() {
		Model.roles.clear();
		Model.users.clear();
		Model.addUser("mitarbeiter", new User("mitarbeiter", 
				"mitarbeiter", Model.mitarbeiterRole, 40));
		Model.addUser("admin", new User("admin", "admin", Model.adminRole, 40));
		Model.addRole("Mitarbeiter", Model.mitarbeiterRole);
		Model.addRole("Admin", Model.adminRole);
		Model.absenceReasons = new Vector<String>(Arrays.asList("Urlaub", "Krankheit"));
		Model.startFlexibility[0] = LocalTime.parse("07:00:00");
		Model.startFlexibility[1] = LocalTime.parse("09:00:00");
		Model.endFlexibility[0] = LocalTime.parse("15:00:00");
		Model.endFlexibility[1] = LocalTime.parse("18:00:00");
		Model.pauseStartEnd[0] = LocalTime.parse("12:00:00");
		Model.pauseStartEnd[1] = LocalTime.parse("12:45:00");
		
		Utilities.defaultDataLoaded = true;
	}
	
//	/**
//	 * Fills and returns a Vector with the LocalDates of the days between the 
//	 * start and end date.
//	 * 
//	 * @param startDate
//	 * @param endDate
//	 * @return returns Vector that contains LocalDates between the given dates
//	 */
//	public static Vector<LocalDate> allDatesBetween(LocalDate startDate, LocalDate endDate) {
//		long daysAway = ChronoUnit.DAYS.between(startDate, endDate);
//		
//		Vector<LocalDate> tmpVec = new Vector<>();
//		// daysAway + 1 so that we include the last day
//		for (int i = 0; i < daysAway + 1; ++i) {
//			tmpVec.add(startDate.plusDays(i));
//		}
//		return tmpVec;
//	}
//	
//	/**
//	 * Using the given formatter and the reason returns entries between the start and 
//	 * the end date in one Vector. Entries are of the form (Date, reason, reason). 
//	 * Reason is written instead of the start and the end time.
//	 * 
//	 * @param startDate
//	 * @param endDate
//	 * @param reason
//	 * @param formatter
//	 * @return returns a Vector that contains entries between the start and end date
//	 */
//	public static Vector<Vector<String>> allEntriesBetween(LocalDate startDate, 
//			LocalDate endDate, String reason, DateTimeFormatter formatter) {
//		long daysAway = ChronoUnit.DAYS.between(startDate, endDate);
//		Vector<Vector<String>> tmpVec = new Vector<>();
//		// daysAway + 1 so that we include the last day
//		for (int i = 0; i < daysAway + 1; ++i) {
//			tmpVec.add(new Vector<String>( Arrays.asList(
//					startDate.plusDays(i).format(formatter),
//					reason,
//					reason) ) );
//		}
//		return tmpVec;
//	}
	
}
