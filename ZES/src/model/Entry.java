package model;

import java.time.LocalDate;

/**
 * Represents an entry in the system.
 */
public class Entry implements Comparable<Entry> {
	private LocalDate date;
	
	private boolean note = false;
	
	/**
	 * Constructs and entry with the given date
	 * 
	 * @param date date on which the user worked
	 * @param startTime time when the user came to work
	 * @param endTime time when user went from work
	 */
	Entry(LocalDate date) {
		this.date = date;
	}

	@Override
	public int compareTo(Entry other) {
		if (date.isBefore(other.getDate())) 
			return -1;
		else if (date.isEqual(other.getDate()))
			return 0;
		else 
			return 1;
	}
	
	/**
	 * @return the date
	 */
	public LocalDate getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(LocalDate date) {
		this.date = date;
	}

	/**
	 * @return the note
	 */
	public boolean isNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(boolean note) {
		this.note = note;
	}
	
	
}
