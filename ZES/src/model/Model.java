package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.Key;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Vector;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Model of the data. Contains data structures which represent different parts
 * of the system 
 */
public class Model {
	
	// Default values are the same as those given to us
	// in the letter
	public static LocalTime startFlexibility[] = new LocalTime[2];
	public static LocalTime endFlexibility[] = new LocalTime[2];
	public static LocalTime pauseStartEnd[] = new LocalTime[2];
	static {
		startFlexibility[0] = LocalTime.parse("07:00:00");
		startFlexibility[1] = LocalTime.parse("09:00:00");
		endFlexibility[0] = LocalTime.parse("15:00:00");
		endFlexibility[1] = LocalTime.parse("18:00:00");
		pauseStartEnd[0] = LocalTime.parse("12:00:00");
		pauseStartEnd[1] = LocalTime.parse("12:45:00"); 
	}
	
	public static Vector<String> absenceReasons = new Vector<String>(Arrays.asList("Urlaub", "Krankheit"));
	
	public static HashMap<String, User> users = new HashMap<>();
	public static HashMap<String, Role> roles = new HashMap<>();
		
	public static Role mitarbeiterRole = new Role("Mitarbeiter", true, true, true, false, false, false);
	public static Role adminRole = new Role("Admin", true, true, true, true, true, true);
	
	public static User activeUser = null;
	public static User chosenUser = null;
	// Active user wants to see his/her own data or someone else's
	public static boolean ownData = false;
	
	/**
	 * Reads system data from a file with the given filename.
	 * File should be in resources folder.
	 * 
	 * @param filename
	 */
	public static void readFromFile(String filename) {
		
		try (BufferedReader in = 
				new BufferedReader(new FileReader("resources/"+filename), 32*1024))
		{
			String line;
			String values[];
			// Read roles
			while (!"USERS".equals((line = in.readLine()))) {
				values = line.split(" ");
				String rolename = values[0];
				boolean accRights[] = new boolean[6];
				for (int i = 1; i < values.length; ++i) {
					accRights[i-1] = Boolean.parseBoolean(values[i]);
				}
				addRole(rolename, new Role(rolename, accRights));
			}
			while(!"SYSTEM".equals(line = in.readLine())) {
				values = line.split(" ");
				String username = values[0];
				String ciphertext = values[1];
				Cipher aes =  Cipher.getInstance("AES");
				Key encrKey = new SecretKeySpec(Utilities.encryptionKey.getBytes(), "AES");
				aes.init(Cipher.DECRYPT_MODE, encrKey);
				byte decoded[] = Base64.getDecoder().decode(ciphertext);
				String pass = new String(aes.doFinal(decoded));
				Role userRole = Model.roles.get(values[2]);
				int hoursPerWeek = Integer.parseInt(values[3]);
				boolean atWork = Boolean.parseBoolean(values[4]);
				double sumOfWorkTime = Double.parseDouble(values[5]);
				double workTimeStatus = Double.parseDouble(values[6]);
				int daysWorked = Integer.parseInt(values[7]);
				
				User user = new User(username, pass, userRole, hoursPerWeek, 
						sumOfWorkTime, workTimeStatus, daysWorked);
				user.setAtWork(atWork);
				
				int numEntries = Integer.parseInt(values[8]);
				for (int i = 9; i < 9 + 3 * numEntries; i+=3) {
					Entry entry;
					if (values[i+1].substring(2, 3).equals(":")) {
						try {
							entry = new TimeEntry(
									LocalDate.parse(values[i], Utilities.dateFormatter),
									LocalTime.parse(values[i+1], Utilities.timeFormatter),
									LocalTime.parse(values[i+2], Utilities.timeFormatter)
									);
						} catch (DateTimeParseException e){
							entry = new TimeEntry(
									LocalDate.parse(values[i], Utilities.dateFormatter),
									LocalTime.parse(values[i+1], Utilities.timeFormatter),
									null
									);
						}
						
					} else {
						entry = new AbsenceEntry(
								LocalDate.parse(values[i], Utilities.dateFormatter),
								values[i+1]);
					}
					
					user.addEntryNoStats(entry);
				}
				
				addUser(username, user);
			}
			
			if (null != (line = in.readLine())) {
				absenceReasons.clear();
				values = line.split(" ");
				for (String value : values) {
					absenceReasons.add(value);
				}
			}
			if (null != (line = in.readLine())) {
				values = line.split(" ");
				startFlexibility[0] = LocalTime.parse(values[0], Utilities.timeFormatter);
				startFlexibility[1] = LocalTime.parse(values[1], Utilities.timeFormatter);
			}
			if (null != (line = in.readLine())) {
				values = line.split(" ");
				endFlexibility[0] = LocalTime.parse(values[0], Utilities.timeFormatter);
				endFlexibility[1] = LocalTime.parse(values[1], Utilities.timeFormatter);
			}
			if (null != (line = in.readLine())) {
				values = line.split(" ");
				pauseStartEnd[0] = LocalTime.parse(values[0], Utilities.timeFormatter);
				pauseStartEnd[1] = LocalTime.parse(values[1], Utilities.timeFormatter);
			}
			
		// Extensions that should make the application more robust
 		} catch (IOException e) {
 			System.out.println("File not found, starting program with "
 					+ "default values");
 			Utilities.loadDefaultData();
 		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception occured while reading the file, starting program with "
 					+ "default values");
 			Utilities.loadDefaultData();
		}
	}
	
	/**
	 * Stores system data to a file with the given filename.
	 * File should be in resources folder.
	 * 
	 * In the file every line corresponds to a different role or user.
	 * After the line containing only 'USERS', on the following line every line 
	 * corresponds a different role.
	 * Lines look like this:
	 * "username encryptedPassword role hoursPerWeek atWork sumOfWorkTime 
	 * workTimeStatus daysWorked #entriesInData entry1 ..."
	 * 
	 * After the line containing 'SYSTEM' information related to the flexibility
	 * limits and reasons of absence is written as follows:
	 * line 1 - absence reasons 		"reason reson reason ..."
	 * line 2 - start time flexibility 	"lowerLimit upperLimit"
	 * line 3 - end time flexibility 	"lowerLimit upperLimit"
	 * line 4 - pause 					"lowerLimit upperLimit"
	 * 
	 * @param filename
	 */
	public static void writeToFile(String filename) {
		
		String path;
		
		// Extension that should make the application more robust
		// If default data was loaded, writing to the initial file is avoided
		// and data is written to another file that corresponds to the last 
		// faulty session.
		if (Utilities.defaultDataLoaded) {
			path = "resources/lastFaultySession.txt";
		} else {
			path = "resources/" + filename;
		}
		Key encrKey = new SecretKeySpec(Utilities.encryptionKey.getBytes(), "AES");
		try (BufferedWriter out = 
				new BufferedWriter(new FileWriter(path), 32 * 1024))
		{
			Cipher aes = Cipher.getInstance("AES");
			aes.init(Cipher.ENCRYPT_MODE, encrKey);
			
			for (Role role : Model.roles.values()) {
				out.write(role.getName() + " " + role.startEndTimeDocumentation + " ");
				out.write(role.absenceReasonInput + " " + role.accessData + " ");
				out.write(role.userAdministration + " " + role.systemAdjustment);
				out.write(" " + role.permissionsAdministration);
				
				out.newLine();
			}
			out.write("USERS");
			out.newLine();
			for (User user : Model.users.values()) {
				byte[] pass = aes.doFinal(user.getPassword().getBytes());
				String ciphertext = Base64.getEncoder().encodeToString(pass);
				ciphertext = ciphertext.replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", "");
				out.write(user.getName() + " " + new String(ciphertext) + " ");
				out.write(user.getRole().getName() + " " + user.getHoursPerWeek());
				out.write(" " + user.isAtWork() + " " + user.getSumOfWorkTime() + " ");
				out.write(user.getWorkTimeStatus() + " " + user.getDaysWorked() + " ");
				out.write(user.getEntries().size() + " ");
				for (Entry entry : user.getEntries().values()) {
					if (entry instanceof TimeEntry) {
						TimeEntry timeEntry = (TimeEntry) entry;
						try {
							out.write(timeEntry.getDate().format(Utilities.dateFormatter) + " " + 
									timeEntry.getStartTime().format(Utilities.timeFormatter) + " " + 
									timeEntry.getEndTime().format(Utilities.timeFormatter) + " ");
						} catch (NullPointerException e) {
							out.write(timeEntry.getDate().format(Utilities.dateFormatter) + " " + 
									timeEntry.getStartTime().format(Utilities.timeFormatter) + " " + 
									"null" + " ");
						}
					} else {
						AbsenceEntry absenceEntry = (AbsenceEntry) entry;
						out.write(absenceEntry.getDate().format(Utilities.dateFormatter) + 
								" " + absenceEntry.getAbsenceReason() + 
								" " + absenceEntry.getAbsenceReason() + " ");
					}
										
				}
				out.newLine();
			}
			out.write("SYSTEM");
			out.newLine();
			for (String reason : absenceReasons) {
				out.write(reason + " ");
			}
			out.newLine();
			out.write(startFlexibility[0].format(Utilities.timeFormatter) 
					+ " " + startFlexibility[1].format(Utilities.timeFormatter));
			out.newLine();
			out.write(endFlexibility[0].format(Utilities.timeFormatter) 
					+ " " + endFlexibility[1].format(Utilities.timeFormatter));
			out.newLine();
			out.write(pauseStartEnd[0].format(Utilities.timeFormatter) 
					+ " " + pauseStartEnd[1].format(Utilities.timeFormatter));
			
		} catch (Exception e) {
			e.printStackTrace();
		} 	
		
	}
	
	/**
	 * Adds the user with the specified username to the system.
	 * 
	 * @param username
	 * @param user object that represents the user
	 * @return returns false if the user already exists
	 */
	public static boolean addUser(String username, User user) {
		if (!users.keySet().contains(username)) {
			users.put(username, user);
			// Add an entry to the user table as well
			Utilities.usersTable.add(new Vector<String>(Arrays.asList(username, 
					user.getRole().getName(),
					Integer.toString(user.getHoursPerWeek()))));
			return true;
		}
		return false;
	}
	
	/**
	 * Deletes the user from the system.
	 * 
	 * @param username
	 * @return returns false if there was no user with the given username in 
	 * the system
	 */
	public static boolean deleteUser(String username) {
		if (users.remove(username) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * Adds the role with the given name to the system.
	 * 
	 * @param roleName
	 * @param role object that represents the role
	 * @return returns false if the role already exists in the system
	 */
	public static boolean addRole(String roleName, Role role) {
		if (!roles.keySet().contains(roleName)) {
			roles.put(roleName, role);
			// Add an entry to the user table as well
			Utilities.rolesTable.add(new Vector<Object>(Arrays.asList(
					roleName, role.startEndTimeDocumentation, role.absenceReasonInput,
					role.accessData, role.userAdministration, role.systemAdjustment,
					role.permissionsAdministration) ) );
			return true;
		}
		return false;
	}
	
	/**
	 * Deletes the role with the given name from the system.
	 * 
	 * @param roleName
	 * @return returns false if there was no role with the given role name
	 */
	public static boolean deleteRole(String roleName) {
		return roles.remove(roleName) != null;
	}

}
