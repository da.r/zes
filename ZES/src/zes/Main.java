package zes;

import model.Model;

/**
 * Implementation of the Zeiterfassunssystem ZES. 
 * Starting point of the application.
 * 
 * 
 * Remark: 
 * Implementation hat 2 default Benutzer:
 * 		(1) Benutzername: <b>mitarbeiter</b> mit Passwort: <b>mitarbeiter</b> in der Rolle Mitarbeiter
 * 		(2) Benutzername: <b>admin</b> mit Passwort: <b>admin</b> in der Rolle Admin
 */
public class Main {

	
	// Used in testing
	public static Windows activeWindow;
	public static Object activeWindowObject;
	
	public static void main(String[] args) {
		
		// Write system data to file on exit
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run() {	
		    	Model.writeToFile("systemData.txt");
		    }
		});
		
		Model.readFromFile("systemData.txt");
		
		
		
		Main.activeWindowObject = new Authentication();
	}

}
