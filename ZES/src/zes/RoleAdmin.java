package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import model.Model;
import model.Role;
import model.User;
import model.Utilities;
import net.miginfocom.swing.MigLayout;

/**
 * Window that allows administration of the roles.
 */
public class RoleAdmin {
	
	// Attributes that were moved so that we can use them in tests
	JButton backButton;
	JButton addRoleButton;
	JTextField addRoleField;
	JButton deleteRoleButton;
	JTable table;
	
	JLabel infoMsg = new JLabel("");
	JFrame frame;

	public RoleAdmin() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.ROLE_ADMIN;
				
				// Create Frame
				frame = new JFrame("Administration der Berechtigungen");

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				// set layout
				frame.setLayout(new MigLayout("align 50% 50%"));

				// Window Settings
				frame.setPreferredSize(new Dimension(1000, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);				
		
				Vector<String> header = new Vector<>(Arrays.asList(
						"Rollenname", 
						"Zeitdokumentation", 
						"<html><center>Fehlzeitgrund <br/>Eingabe<center></html>", 
						"<html><center>Zugriff <br/>eigene Daten</center><html>", 
						"<html><center>Benutzer- <br/>administration</center></html>", 
						"Systemanpassung", 
						"<html><center>Administration der <br/>Berechtigungen</center></html>"));
				
				
				// Only role name cell should not be editable 
				DefaultTableModel tableModel = new DefaultTableModel(Utilities.rolesTable, header) {
					
					private static final long serialVersionUID = 1L;

					@Override
					public boolean isCellEditable(int row, int col) {
						switch (col) {
							case 0:
								return false;
							default:
								return true;
						}
					}
				};
								
				table = new JTable(tableModel) {
					
                    private static final long serialVersionUID = -1067519658919707616L;

                    @Override
					public Class getColumnClass(int column) {
						switch(column) {
							case 0:
								return String.class;
							default:
								return Boolean.class;
						}
					}
				};
				
				
				// Allow selection of only one row
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				
				DefaultTableCellRenderer tableRenderer = 
				        ((DefaultTableCellRenderer) table.getTableHeader()
				                                         .getDefaultRenderer());
				tableRenderer.setHorizontalAlignment(JLabel.CENTER);
				tableRenderer.setVerticalAlignment(JLabel.CENTER);
				
				backButton = new JButton("Zurück");
				backButton.setPreferredSize(new Dimension(150, 40));
				
				addRoleButton = new JButton("Rolle hinzufügen");
				addRoleButton.setPreferredSize(new Dimension(150, 40));
				
				addRoleField = new JTextField(15);
				addRoleField.setPreferredSize(new Dimension(150, 40));
				
				deleteRoleButton = new JButton("Eintrag löschen");
				deleteRoleButton.setPreferredSize(new Dimension(150, 40));
				
				JScrollPane tablePane = new JScrollPane(table);
				tablePane.setPreferredSize(new Dimension(1000, 250));
				
				tablePane.setColumnHeader(new JViewport() {
				    
                    private static final long serialVersionUID = 9121502796349859584L;

                    @Override 
					public Dimension getPreferredSize() {
						Dimension dim = super.getPreferredSize();
						dim.height = 50;
						return dim;
					}
				});
			
				frame.add(tablePane, "cell 0 0, span");
				
				frame.add(deleteRoleButton, "cell 0 1");
				
				frame.add(addRoleButton, "cell 1 1");
				frame.add(addRoleField, "cell 2 1");
				
				frame.add(backButton, "cell 0 2");
				
				frame.add(infoMsg, "cell 1 2, span");
				
				
				// Listen to the changes in table and react
				table.getModel().addTableModelListener(new TableModelListener() {

					public void tableChanged(TableModelEvent event) {
						for (Vector<Object> row : Utilities.rolesTable) {
							String roleName = (String)row.get(0);
							Role role = Model.roles.get(roleName);
							role.startEndTimeDocumentation = (boolean) row.get(1);
							role.absenceReasonInput = (boolean) row.get(2);
							role.accessData = (boolean) row.get(3);
							role.userAdministration = (boolean) row.get(4);
							role.systemAdjustment = (boolean) row.get(5);
							role.permissionsAdministration = (boolean) row.get(6);
						}
					}
				});
				
				deleteRoleButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						// remove the selected row
						int selectedRow = table.getSelectedRow();
						
						// Extension that should make the application more robust	
						// User cannot delete role if some user has this role
						String selectedRole = (String)Utilities.rolesTable.get(selectedRow).get(0);
						Set<String> rolesWithUsers = new HashSet<>();
						for (User user : Model.users.values()) {
							rolesWithUsers.add(user.getRole().getName());
						}
						if (!rolesWithUsers.contains(selectedRole)) {
							tableModel.removeRow(selectedRow);
							Model.deleteRole(selectedRole);
						}
						else {
							infoMsg.setText("Wenn ein Benutzer in der Rolle existiert, "
									+ "kann die Rolle nicht gelöscht werden.");
						}
						
					}
				});
				
				addRoleButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						String roleToAdd = addRoleField.getText();
						
						if (Model.roles.containsKey(roleToAdd)) {
							infoMsg.setText("<html>Rolle <b>" + 
									roleToAdd + "</b> existiert schon</html>");
						} else {
							Model.addRole(roleToAdd, new Role(roleToAdd));
							tableModel.fireTableDataChanged();
						}
						
					}
				});
				
				backButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {						
						frame.setVisible(false);
						Main.activeWindowObject = new StartWindow();
						
					}
				});
				
			}

		});
	}
}
