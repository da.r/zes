package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.Model;
import model.Utilities;
import net.miginfocom.swing.MigLayout;

/**
 * Window that allows customization of the system.
 * It allows the user to change the flexibility limits of the start and end time, 
 * as well as start and end of the pause.
 * It also allows the user to add or delete reasons of absence.
 *
 * Remark: 
 * Implementierung hat 2 default Benutzer:
 * 		(1) Benutzername: <b>mitarbeiter</b> mit Passwort: <b>mitarbeiter</b> in der Rolle Mitarbeiter
 * 		(2) Benutzername: <b>admin</b> mit Passwort: <b>admin</b> in der Rolle Admin
 */
public class SystemCustomization {

	
	// Attributes that were moved so that we can use them in tests
	JComboBox<String> flexibilityList;
	JSpinner lowerLimitSpinner;
	JSpinner upperLimitSpinner;
	JButton changeFlexiButton;
	JButton backButton;
	
	JButton addReasonButton;
	JTextField addReasonField;
	
	JButton deleteReasonButton;
	JComboBox<String> deleteReasonList;
	
	JLabel infoMsg = new JLabel("");
	JFrame frame;
	
	// Internal values converted to Date
	private Date startFlexiLower;
	private Date startFlexiUpper;
	private Date endFlexiLower;
	private Date endFlexiUpper;
	private Date pauseLower;
	private Date pauseUpper; 

	public SystemCustomization() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.SYSTEM_CUSTOMIZATION;

				// Create Frame
				frame = new JFrame("Systemanpassung");

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// Layout Window
				frame.setLayout(new MigLayout(
						"align 50% 50%, insets 30 20 15 0",
						"",
						"[][]40[][][]10[]"
						));
				

				// Objects of the Frame
				JLabel username = new JLabel("Flexibilitätgrenzen");
				JLabel lower = new JLabel("Untere Grenze");
				JLabel upper = new JLabel("Obere Grenze");
				
				flexibilityList = new JComboBox<String>(new String[] {"Kommzeit", "Gehzeit", "Pause"} );
		
				SpinnerDateModel lowerLimitModel = new SpinnerDateModel();
				lowerLimitModel.setCalendarField(Calendar.MINUTE);

				lowerLimitSpinner = new JSpinner(lowerLimitModel);
				lowerLimitSpinner.setEditor(new JSpinner.DateEditor(lowerLimitSpinner, "HH:mm:ss"));
				
				SpinnerDateModel upperLimitModel = new SpinnerDateModel();
				upperLimitModel.setCalendarField(Calendar.MINUTE);
				
				upperLimitSpinner = new JSpinner(upperLimitModel);
				upperLimitSpinner.setEditor(new JSpinner.DateEditor(upperLimitSpinner, "HH:mm:ss"));
				
				// Convert to Date because Spinners take time Date types
				updateDateVariables();
				
				// Set default values in the spinners so that they correspond to the 
				// current values in the flexibility limits
				if ("Kommzeit".equals(flexibilityList.getSelectedItem().toString())) {
					lowerLimitSpinner.setValue(startFlexiLower);
					upperLimitSpinner.setValue(startFlexiUpper);
				} else if ("Gehzeit".equals(flexibilityList.getSelectedItem().toString())) {
					lowerLimitSpinner.setValue(endFlexiLower);
					upperLimitSpinner.setValue(endFlexiUpper);
				}  else {
					lowerLimitSpinner.setValue(pauseLower);
					upperLimitSpinner.setValue(pauseUpper);
				}
				
				addReasonButton = new JButton("Fehlzeitschlüssel hinzufügen");
				addReasonButton.setPreferredSize(new Dimension(240, 20));
				addReasonField = new JTextField(10);
				
				deleteReasonButton = new JButton("Lösche ausgewählten"
				        + " Fehlzeitschlüssel");
				deleteReasonButton.setPreferredSize(new Dimension(240, 20));
				deleteReasonList = new JComboBox<>(Model.absenceReasons);
				
				changeFlexiButton = new JButton("Ändere");
				backButton = new JButton("Zurück");
				backButton.setPreferredSize(new Dimension(170, 30));
				
				frame.add(addReasonButton, "cell 0 0, span 3, align right");
				frame.add(addReasonField, "cell 3 0, span 2");
				
				frame.add(deleteReasonButton, "cell 0 1, span 3, align right");
				frame.add(deleteReasonList, "cell 3 1, span 2");
				
				frame.add(username, "cell 0 2");
				frame.add(flexibilityList, "cell 0 3, align center, span 2");
				frame.add(backButton, "cell 0 4, span");
				frame.add(lower, "cell 2 2, align center");
				frame.add(upper, "cell 3 2");
				frame.add(lowerLimitSpinner, "cell 2 3, align center");
				frame.add(upperLimitSpinner, "cell 3 3");
				frame.add(changeFlexiButton, "cell 2 4, align center");
				frame.add(infoMsg, "cell 0 5, span");

				// Window Settings
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);

				// Event when clicking on "Bestätigen" button
				changeFlexiButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						
						
						Date lower = (Date) lowerLimitSpinner.getValue();
						LocalTime lowerLimit = LocalDateTime.ofInstant(lower.toInstant(), 
								ZoneId.systemDefault()).toLocalTime();
						Date upper = (Date) upperLimitSpinner.getValue();
						LocalTime upperLimit = LocalDateTime.ofInstant(upper.toInstant(), 
								ZoneId.systemDefault()).toLocalTime();
						// If lower limit is after the upper limit do not change
						// the flexibility limits
						if (lower.after(upper)) {
							// Extension that should make the application more robust	
							infoMsg.setText("Untere Grenze ist nach der oberen Grenze.");
							return;
						}
						if ("Kommzeit".equals(flexibilityList.getSelectedItem().toString())) {
							Model.startFlexibility[0] = lowerLimit;
							Model.startFlexibility[1] = upperLimit;
							infoMsg.setText("Flexibilitätsgrenzen für die Kommzeit sind geändert.");
						} else if ("Gehzeit".equals(flexibilityList.getSelectedItem().toString())) {
							Model.endFlexibility[0] = lowerLimit;
							Model.endFlexibility[1] = upperLimit;
							infoMsg.setText("Flexibilitätsgrenzen für die Gehzeit sind geändert.");
						} else {
							Model.pauseStartEnd[0] = lowerLimit;
							Model.pauseStartEnd[1] = upperLimit;
							infoMsg.setText("Pause ist geändert.");
						}
						

					}
				});
				
				// Event when clicking on "Zurueck" button
				backButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new StartWindow();

					}
				});
				
				// Event when changing combobox
				// Set default entries in the spinners to the corresponding
				// current flexibility limits
				flexibilityList.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						
						// Update values in Date variables
						updateDateVariables();
						
						if ("Kommzeit".equals(flexibilityList.getSelectedItem().toString())) {
							lowerLimitSpinner.setValue(startFlexiLower);
							upperLimitSpinner.setValue(startFlexiUpper);
						} else if ("Gehzeit".equals(flexibilityList.getSelectedItem().toString())) {
							lowerLimitSpinner.setValue(endFlexiLower);
							upperLimitSpinner.setValue(endFlexiUpper);
						}  else {
							lowerLimitSpinner.setValue(pauseLower);
							upperLimitSpinner.setValue(pauseUpper);
						}

					}
				});
				

				
				addReasonButton.addActionListener(new ActionListener() {

					@Override		
					public void actionPerformed(ActionEvent e) {
						
						String newReasonStr = addReasonField.getText();
						
						if (!Model.absenceReasons.contains(newReasonStr))
							Model.absenceReasons.add(newReasonStr);
						
						infoMsg.setText("<html>Fehlzeitgrund <b>" + 
								newReasonStr + "</b> hinzugefügt.</html>");

					}
				});
				
				
				deleteReasonButton.addActionListener(new ActionListener() {

					@Override		
					public void actionPerformed(ActionEvent e) {
						
						String reasonToDel = deleteReasonList.getSelectedItem().toString();
						
						Model.absenceReasons.remove(reasonToDel);
						infoMsg.setText("<html>Fehlzeitgrund <b>" +
								reasonToDel + "</b> gelösch.");

					}
				});
			}
		});
	}
	
	/**
	 * Helper function that updates variables of type Date that are used 
	 * in this window.
	 */
	private void updateDateVariables() {
		try {
			startFlexiLower = Utilities.timeFormat.parse(
					Model.startFlexibility[0].format(Utilities.timeFormatter)
				);
			startFlexiUpper = Utilities.timeFormat.parse(
					Model.startFlexibility[1].format(Utilities.timeFormatter)
				);
			endFlexiLower = Utilities.timeFormat.parse(
					Model.endFlexibility[0].format(Utilities.timeFormatter)
				);
			endFlexiUpper = Utilities.timeFormat.parse(
					Model.endFlexibility[1].format(Utilities.timeFormatter)
				);
			pauseLower = Utilities.timeFormat.parse(
					Model.pauseStartEnd[0].format(Utilities.timeFormatter)
				);
			pauseUpper = Utilities.timeFormat.parse(
					Model.pauseStartEnd[1].format(Utilities.timeFormatter)
				);
		} catch (ParseException pe) {
			pe.getStackTrace();
		}
	}	
}
