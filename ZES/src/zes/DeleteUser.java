package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

import model.Model;
import model.Utilities;
import net.miginfocom.swing.MigLayout;

/**
 * Window that allows the user to delete an existing user.
 */
public class DeleteUser {
	
	// Attributes that were moved so that we can use them in tests
	JButton backButton;
	JButton deleteUserButton;
	
	JTable table;
	JFrame frame;

	public DeleteUser() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.DELETE_USER;
				
				// Create Frame
				frame = new JFrame("Benutzer löschen");

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				// set layout
				frame.setLayout(new MigLayout("align 50% 50%"));

				// Window Settings
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);

				Vector<String> columnTitles = new Vector<>(
						Arrays.asList("Benutzername", "Rolle", "Wochenstunden"));
				
				
				// Table model with cells that are not editable
				DefaultTableModel tableModel = new DefaultTableModel(Utilities.usersTable, columnTitles) {
					
					private static final long serialVersionUID = 1L;

					@Override
				    public boolean isCellEditable(int row, int column) {
				        return false;
				    }
				};
								
				table = new JTable(tableModel);
				// Allow selection of only one row
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				
				backButton = new JButton("Zurück");
				backButton.setPreferredSize(new Dimension(150, 40));
				
				deleteUserButton = new JButton("Eintrag löschen");
				deleteUserButton.setPreferredSize(new Dimension(150, 40));
				
				JLabel errorMsg = new JLabel("");

				JScrollPane tablePane = new JScrollPane(table);
				
				frame.add(tablePane, "cell 0 0, span");
				frame.add(backButton, "cell 0 1");
				frame.add(deleteUserButton, "cell 1 1");
				frame.add(errorMsg, "cell 0 2, span");
				
				deleteUserButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						// remove the selected row
						int selectedRow = table.getSelectedRow();
						String userToRemove = (String) table.getValueAt(selectedRow, 0);
						// check whether this user is active at the moment
						if (Model.activeUser.getName().equals(userToRemove)) {
							// Extension that should make the application more robust
							errorMsg.setText("Benutzer kann sich nicht selbst löschen.");
							return;
						}
						
						Model.deleteUser((String)table.getValueAt(selectedRow, 0));
						tableModel.removeRow(selectedRow);
						
					}
				});
				
				backButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {						
						frame.setVisible(false);
						Main.activeWindowObject = new UserAdminWindow();
						
					}
				});
				
			}

		});
	}
}
