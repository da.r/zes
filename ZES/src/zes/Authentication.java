package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.Model;
import model.User;
import net.miginfocom.swing.MigLayout;


/**
 * Authentication, shows the login screen.
 */
public class Authentication {

	// Attributes that were moved so that we can use them in tests
	JTextField usernameField;
	JPasswordField passwordField;
	JButton loginButton;
	
	boolean falseUsernamePassMessage = true;
	JLabel falseUnamePassword;
	
	JFrame frame;

	public Authentication() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}

				// Set active window
				Main.activeWindow = Windows.AUTHENTICATION;
				
				// Create Frame
				frame = new JFrame("Zeiterfassungssystem ZES");

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// Layout Window
				frame.setLayout(new MigLayout(
							"align 50% 0%, insets 20",	// Layout constraints
							"[right][][]",		// Column constraints
							"[]20[][]10"		// Row Constraints
							));

				// Objects of the Frame
				BufferedImage logo;
				JLabel logoLabel = null;
				try {
					logo = ImageIO.read(getClass().getResource("resources/Logo.png"));
					logoLabel = new JLabel(new ImageIcon(logo.getScaledInstance(100, 100, Image.SCALE_SMOOTH)));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				JLabel username = new JLabel("Benutzername:");
				JLabel password = new JLabel("Passwort:");
				usernameField = new JTextField(20);
				passwordField = new JPasswordField(20);
				loginButton = new JButton("Login");
				loginButton.setPreferredSize(new Dimension(250, 50));
				
				if (logoLabel != null)
					frame.add(logoLabel, "span, align center, wrap");
				
				frame.add(username);
				frame.add(usernameField, "wrap");
				frame.add(password);
				frame.add(passwordField, "wrap");
				frame.add(loginButton, "span, align center");
							

				// Window Settings
				frame.setPreferredSize(new Dimension(300, 400));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);

				
				loginButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						String uNameString = usernameField.getText();
						String pass = new String(passwordField.getPassword());
						

						if (falseUsernamePassMessage) {
								falseUnamePassword = new JLabel("Falsche Benutzername oder Passwort");
								frame.add(falseUnamePassword, "span");
								frame.setVisible(true);
								falseUsernamePassMessage = false;
						}
						
						if (Model.users.containsKey(uNameString)) {
							User userObject = Model.users.get(uNameString);
							if (pass.equals(userObject.getPassword())) {
								
								Model.activeUser = userObject;
								
								frame.setVisible(false);
								Main.activeWindowObject = new StartWindow();
							}
						}
						
					
					}
				});
				;
			}
		});
	}
}