package zes;

/**
 * Windows that are contained in the system. Used to represent an active window 
 * and that way allow us to test the current active window.
 */
public enum Windows {
	
	AUTHENTICATION,
	START_WINDOW,
	MITARBEITER_START,
	FEHLZEITGRUND_EINGABE,
	LATE_INPUT,
	DATA_WINDOW,
	STARTENDTIMEDOCUMENTED,
	USER_ADMIN,
	SYSTEM_CUSTOMIZATION,
	ROLE_ADMIN, 
	ADD_USER,
	DELETE_USER,
	STATISTIKEN
}