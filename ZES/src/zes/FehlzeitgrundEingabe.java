package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.toedter.calendar.JDateChooser;

import model.Model;
import net.miginfocom.swing.MigLayout;

/**
 * Window that allows the user to enter the reason for absence.
 */
public class FehlzeitgrundEingabe {

	// Attributes that were moved so that we can use them in tests
	JDateChooser startDatePicker;
	JDateChooser endDatePicker;
	JComboBox<String> grundList;
	JButton okButton;
	JButton backButton;
	
	JLabel falseEntryMsg;
	boolean falseEntryMsgDrawn = false;
	
	JFrame frame;

	public FehlzeitgrundEingabe() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.FEHLZEITGRUND_EINGABE;

				// Create Frame
				frame = new JFrame("Fehlzeitgrundeingabe");
				
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// Layout Window
				frame.setLayout(new MigLayout("align 50% 50%, insets 20"));

				// Objects of the Frame
				JLabel startDateLabel = new JLabel("Anfangsdatum");
				JLabel endDateLabel = new JLabel("Enddatum");
								
				startDatePicker = new JDateChooser(new Date());
				startDatePicker.setLocale(Locale.GERMANY);
				startDatePicker.setPreferredSize(new Dimension(120, 25));
								
				endDatePicker = new JDateChooser(new Date());
				endDatePicker.setLocale(Locale.GERMANY);
				endDatePicker.setPreferredSize(new Dimension(120, 25));
				
				JLabel grund = new JLabel("Grund");
			    grundList = new JComboBox<String>(Model.absenceReasons);

				okButton = new JButton("Bestätigen");
				backButton = new JButton("Zurück");
			
				frame.add(startDateLabel, "cell 0 0");
				frame.add(startDatePicker, "cell 0 1");
				
				frame.add(endDateLabel, "cell 2 0");
				frame.add(endDatePicker, "cell 2 1");
				
				frame.add(grund, "cell 4 0");
				frame.add(grundList, "cell 4 1");
				
				frame.add(okButton, "cell 2 5");
				frame.add(backButton, "cell 0 5");
				
				//Window Settings
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);
				
				
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						// Write false entry message if it is not already written
						if (!falseEntryMsgDrawn) {
							// Extension that should make the application more robust
							falseEntryMsg = new JLabel("Falsche Eingabe. Anfangszeit soll bevor Endzeit sein.");
							frame.add(falseEntryMsg, "cell 0 7, span");
							frame.setVisible(true);
							falseEntryMsgDrawn = true;
						}
						
						Date startDate = startDatePicker.getDate();
						Date endDate = endDatePicker.getDate();
						
						if (!endDate.before(startDate)) {
							frame.setVisible(false);
							Main.activeWindowObject = new StartWindow();
							
							String selectedReason = grundList.getSelectedItem().toString();
							
							// Convert it to LocalDate so that we can compare them with those in 
							// the database
							LocalDate startDtLocal = LocalDateTime.ofInstant(startDate.toInstant(), 
									ZoneId.systemDefault()).toLocalDate();
							LocalDate endDtLocal = LocalDateTime.ofInstant(endDate.toInstant(), 
									ZoneId.systemDefault()).toLocalDate();
							
							LocalDate currentDate = startDtLocal;
							while (!currentDate.isAfter(endDtLocal)) {
								model.Entry currentEntry = Model.activeUser.getEntries().get(currentDate);
								if (!(currentEntry instanceof model.TimeEntry)) {
									Model.activeUser.addEntryOverwrite(new model.AbsenceEntry(currentDate, selectedReason));
								}
								currentDate = currentDate.plusDays(1);
							}
																							
						}
						
					}
						
				});
				
				backButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new StartWindow();
						
					}
				});
				

			}
		});
	}
}