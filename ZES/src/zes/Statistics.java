package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.toedter.calendar.JDateChooser;

import model.Model;
import model.Utilities;
import net.miginfocom.swing.MigLayout;

/**
 * Window that allows the user to see his statistics in the given time interval.
 *
 */
public class Statistics {

	// Attributes that were moved so that we can use them in tests
	JDateChooser intervalStartPicker;
	JDateChooser intervalEndPicker;
	JButton okButton;
	JButton backButton;
	
	JLabel sumOfWorkTimeStat;
	JLabel workTimePerDayStat;
	JLabel workTimeStatusStat; 
	
	JLabel infoMsg = new JLabel("");
	JFrame frame;

	public Statistics() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.STATISTIKEN;

				// Create Frame
				frame = new JFrame("Statistiken");
				
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// Layout Window
				frame.setLayout(new MigLayout("align 50% 50%, insets 20", 
						"[right][][]", 
						"[][][]50[][]"));

				// Objects of the Frame
				JLabel sumOfWorkTime = new JLabel("Summe der gearbeiteten Zeiten: ");
				JLabel workTimePerDay = new JLabel("Durchschnittliche Arbeitszeit pro Tag: ");
				JLabel hoursState = new JLabel("Stundenstand im Bezug auf gegebenen Zeitraum: ");
				
				String toolTipString = "Positiv wenn der Benutzer Ueberstunden hat, negativ "
						+ "wenn er Unterstunden hat.";
				hoursState.setToolTipText(toolTipString);
				
				sumOfWorkTimeStat = new JLabel("       ");
				workTimePerDayStat = new JLabel("       ");
				workTimeStatusStat = new JLabel("       ");
				workTimeStatusStat.setToolTipText(toolTipString);
				
				JLabel timeIntervalLabel = new JLabel("<html><b>Zeitraum: </b><html>");
				JLabel intervalStartLabel = new JLabel("Anfangsdatum");
				JLabel intervalEndLabel = new JLabel("Enddatum");
								
				intervalStartPicker = new JDateChooser(new Date());
				intervalStartPicker.setLocale(Locale.GERMANY);
				intervalStartPicker.setPreferredSize(new Dimension(120, 25));
								
				intervalEndPicker = new JDateChooser(new Date());
				intervalEndPicker.setLocale(Locale.GERMANY);
				intervalEndPicker.setPreferredSize(new Dimension(120, 25));

				okButton = new JButton("Bestätigen");
				backButton = new JButton("Zurück");
				
				frame.add(sumOfWorkTime, "cell 0 0, span 4");
				frame.add(workTimePerDay, "cell 0 1, span 4");
				frame.add(hoursState, "cell 0 2, span 4");
				
				frame.add(sumOfWorkTimeStat, "cell 5 0, align right");
				frame.add(workTimePerDayStat, "cell 5 1, align right");
				frame.add(workTimeStatusStat, "cell 5 2, align right");
				
				frame.add(timeIntervalLabel, "cell 1 4");
			
				frame.add(intervalStartLabel, "cell 2 3");
				frame.add(intervalStartPicker, "cell 2 4");
				
				frame.add(intervalEndLabel, "cell 3 3");
				frame.add(intervalEndPicker, "cell 3 4");
				
				frame.add(okButton, "cell 3 5");
				frame.add(backButton, "cell 2 5");
				
				frame.add(infoMsg, "cell 0 6, span 5");
				

				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);
				
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						// Start time can not be after end time
						Date startDate = intervalStartPicker.getDate();
						Date endDate = intervalEndPicker.getDate();
						if (startDate.after(endDate)) {
							// Extension that should make the application more robust	
							infoMsg.setText("Anfangsdatum ist nach dem Enddatum.");
							return;
						} else {
							infoMsg.setText("Statistiken für " + 
									Utilities.dateFormat.format(startDate) + " - " +
									Utilities.dateFormat.format(endDate));
							
							LocalDate start = LocalDateTime.ofInstant(startDate.toInstant(), 
									ZoneId.systemDefault()).toLocalDate();
							LocalDate end = LocalDateTime.ofInstant(endDate.toInstant(), 
									ZoneId.systemDefault()).toLocalDate();
							
							double stats[] = Model.activeUser.calculateStatistics(start, end);
							
							sumOfWorkTimeStat.setText(String.format(
									Locale.GERMAN,
									"% 10.3f h",
									stats[0]
							));
							workTimePerDayStat.setText(String.format(
									Locale.GERMAN,
									"% 10.3f h",
									stats[1]
							));
							workTimeStatusStat.setText(String.format(
									Locale.GERMAN,
									"% 10.3f h",
									stats[2]
							));							
						}
						
					}	
				});
				
				backButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new StartWindow();
						
					}
				});
				

			}
		});
	}
}
