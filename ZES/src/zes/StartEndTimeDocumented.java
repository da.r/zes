package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.Model;
import model.TimeEntry;
import model.Utilities;

/**
 * Window that informs the user that s/he documented the Komm- or Gehzeit.
 * 
 * Remark: 
 * Implementierung hat 2 default Benutzer:
 * 		(1) Benutzername: <b>mitarbeiter</b> mit Passwort: <b>mitarbeiter</b> in der Rolle Mitarbeiter
 * 		(2) Benutzername: <b>admin</b> mit Passwort: <b>admin</b> in der Rolle Admin
 */
public class StartEndTimeDocumented {

	// Attributes that were moved so that we can use them in tests
	JButton returnToLoginButton;
	JFrame frame;

	public StartEndTimeDocumented() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.STARTENDTIMEDOCUMENTED;

				// Create Frame
				frame = new JFrame("Komm/Gehzeit dokumentiert");

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// Objects of the Frame
				
				LocalDateTime time = LocalDateTime.now();
				
								
				JLabel satz1 = new JLabel();
				JLabel satz2 = new JLabel();

				// Store date into the database
				// If the entry already exists in the form of Fehlzeitgrund, overwrite it
				LocalDate currentDate = time.toLocalDate();
				// Check whether we have to show the start time or end time message
				if (!Model.activeUser.isAtWork()) {
					satz1.setText("Kommzeit dokumentiert um ");
					satz2.setText(time.format(Utilities.timeFormatter));
					Model.activeUser.setAtWork(true);
					
					Model.activeUser.addEntryOverwrite(new TimeEntry(currentDate, 
							time.toLocalTime(),
							null));
				}
				else {
					
					satz1.setText("Gehzeit dokumentiert um ");
					satz2.setText(time.format(Utilities.timeFormatter));
					Model.activeUser.setAtWork(false);
					
					TimeEntry currentTimeEntry = (TimeEntry) Model.activeUser.
							getEntryOnDate(currentDate);
					Model.activeUser.addEntryOverwrite(new TimeEntry(currentDate, 
							currentTimeEntry.getStartTime(), 
							time.toLocalTime()));
				}
				
				returnToLoginButton = new JButton("Zurück zum Login");

				// Layout Window
				frame.setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();

				// GridBagContaints
				gbc.gridx = 1;
				gbc.gridy = 0;
				frame.add(satz1, gbc);
				gbc.gridx = 2;
				gbc.gridy = 0;
				frame.add(satz2, gbc);
				gbc.gridx = 2;
				gbc.gridy = 3;
				frame.add(returnToLoginButton, gbc);

				// Window Settings
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);


				returnToLoginButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Model.activeUser = null;
						Main.activeWindowObject = new Authentication();

					}
				});

				;
			}
		});
	}
}