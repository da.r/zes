package zes;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import model.AbsenceEntry;
import model.Model;
import model.Role;
import model.TimeEntry;
import model.User;
import model.Utilities;


/**
 * Test class of the implementation of 'Zeiterfassunssystem ZES'.
 */
public class TestZES {
	
	Authentication auth;
	
	// To adjust the main thread sleep time quickly
	int threadSleepTime;
	// Thread.sleep() is used, to force the main thread to wait until the next 
	// window is instantiated
	
	int numOfUsersInTestFile = 2;
	int numOfRolesInTestFile = 2;
	
	int numOfUsersInDefault = 2;
	int numOfRolesInDefault = 2;
	
	/**
	 * Resets the environment.
	 * We always start as admin on hers/his start window.
	 */
	@Before
	public void reset() {
		Model.users.clear();
		Model.roles.clear();
		Utilities.usersTable.clear();
		Utilities.rolesTable.clear();
		Model.activeUser = null;
		threadSleepTime = 400;
		Utilities.loadDefaultData();
	}
	
	/**
	 * Helper function, logs in admin user.
	 * 
	 * @throws InterruptedException 
	 */
	public void setUpAdminUser() throws InterruptedException {
		auth = new Authentication();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		
		auth.usernameField.setText("admin");
		auth.passwordField.setText("admin");
		auth.loginButton.doClick();
		Thread.sleep(threadSleepTime);
		auth.frame.dispose();
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
	}
	
	/**
	 * Helper function, logs in mitarbeiter user.
	 * 
	 * @throws InterruptedException 
	 */
	public void setUpMitarbeiterUser() throws InterruptedException {
		auth = new Authentication();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		auth.usernameField.setText("mitarbeiter");
		auth.passwordField.setText("mitarbeiter");
		auth.loginButton.doClick();
		Thread.sleep(threadSleepTime);
		auth.frame.dispose();
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
	}
	
	// Tests related to the Model class
	
	/**
	 * Tests model.Model.readFromFile() function.
	 * Tests reading a correctly formated space separated values file.
	 */
	@Test
	public void testReadFromFile() {
		// Delete default data that was loaded
		Model.users.clear();
		Model.roles.clear();
		Utilities.defaultDataLoaded = false;
		
		Model.readFromFile("tests/testDataRead.txt");
		
		assertEquals(numOfUsersInTestFile, Model.users.size());
		assertEquals(numOfRolesInTestFile, Model.roles.size());
		assertNotEquals(null, Model.users.get("testUser1"));
		assertNotEquals(null, Model.users.get("testUser2"));
		assertEquals(35, Model.users.get("testUser1").getHoursPerWeek());
		assertEquals(38, Model.users.get("testUser2").getHoursPerWeek());
		assertNotEquals(null, Model.roles.get("testRole1"));
		assertNotEquals(null, Model.roles.get("testRole2"));
	}
	
	/**
	 * Tests model.Model.readFromFile() function.
	 * Tests reading a faulty space separated values file.
	 */
	@Test
	public void testReadFromFaultyFile() {
		// in this case default data should not be loaded
		Model.users.clear();
		Model.roles.clear();
		Utilities.defaultDataLoaded = false;
		
		Model.readFromFile("tests/testFaultyData.txt");
		
		// If file could not be read then the default data should 
		// have been loaded.
		assertTrue(Utilities.defaultDataLoaded);
		
		// In this case it should not be written into the given file, 
		// but into 'lastFaultySession.txt'
		Model.writeToFile("notWrittenHere.txt");
		
		Model.readFromFile("lastFaultySession.txt");
		
		assertEquals(numOfUsersInDefault, Model.users.size());
		assertEquals(numOfRolesInDefault, Model.roles.size());
		assertNotEquals(null, Model.users.get("mitarbeiter"));
		assertNotEquals(null, Model.users.get("admin"));
		assertEquals(40, Model.users.get("mitarbeiter").getHoursPerWeek());
		assertEquals(40, Model.users.get("admin").getHoursPerWeek());
		assertNotEquals(null, Model.roles.get("Mitarbeiter"));
		assertNotEquals(null, Model.roles.get("Admin"));
	}
	
	/**
	 * Tests model.Model.readFromFile() function.
	 * Tests reading an empty space separated values file.
	 */
	@Test
	public void testReadFromEmptyFile() {
		// in this case default data should not be loaded
		Model.users.clear();
		Model.roles.clear();
		Utilities.defaultDataLoaded = false;
		
		Model.readFromFile("tests/emptyFile.txt");
		
		// If file could not be read then the default data should 
		// have been loaded.
		assertTrue(Utilities.defaultDataLoaded);
	}
	
	/**
	 * Tests model.Model.readFromFile() function.
	 * Tests reading from a non existing file.
	 */
	@Test
	public void testReadFromNonExistingFile() {
		// in this case default data should not be loaded
		Model.users.clear();
		Model.roles.clear();
		Utilities.defaultDataLoaded = false;

		Model.readFromFile("nonExisting.txt");
		
		// Default data should be loaded
		assertNotEquals(null, Model.roles.get("Admin"));
		assertNotEquals(null, Model.roles.get("Mitarbeiter"));
		assertNotEquals(null, Model.users.get("admin"));
		assertNotEquals(null, Model.users.get("mitarbeiter"));
	}
	
	/**
	 * Tests model.Model.writeToFile() function.
	 * Tests writing to a file.
	 */
	@Test
	public void testWriteToFile() throws IOException {
		
		// in this case default data should not be loaded
		Model.users.clear();
		Model.roles.clear();
		Utilities.defaultDataLoaded = false;
		
		Model.readFromFile("tests/testDataRead.txt");
		Model.writeToFile("tests/testDataWrite.txt");
		
		Model.users.clear();
		Model.roles.clear();
		
		Model.readFromFile("tests/testDataWrite.txt");
		
		assertEquals(numOfUsersInTestFile, Model.users.size());
		assertEquals(numOfRolesInTestFile, Model.roles.size());
		assertNotEquals(null, Model.users.get("testUser1"));
		assertNotEquals(null, Model.users.get("testUser2"));
		assertEquals(35, Model.users.get("testUser1").getHoursPerWeek());
		assertEquals(38, Model.users.get("testUser2").getHoursPerWeek());
		assertNotEquals(null, Model.roles.get("testRole1"));
		assertNotEquals(null, Model.roles.get("testRole2"));
	}
	
	/**
	 * Tests model.Model.deleteUser() function.
	 */
	@Test
	public void testDeleteUserModel() {
		Model.readFromFile("tests/testDataRead.txt");
		Model.deleteUser("testUser1");
		assertEquals(null, Model.users.get("testUser1"));
	}
	
	/**
	 * Tests model.Model.deleteRole() function.
	 */
	@Test
	public void testDeleteRoleModel() {
		Model.readFromFile("tests/testDataRead.txt");
		Model.deleteRole("testRole1");
		assertEquals(null, Model.roles.get("testRole1"));
	}
	
	
	// Tests related to the User class
	
	/**
	 * Tests model.User.addEntry() function.
	 * Tests adding a new entry to user data in model.
	 * @throws InterruptedException 
	 */
	@Test
	public void testAddEntryModel() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		int sizeBefore = Model.activeUser.getEntries().size();
		Model.activeUser.addEntry(new TimeEntry(LocalDate.now(), 
				LocalTime.of(12, 15, 00), 
				LocalTime.of(19, 15, 00)));
		
		assertEquals(sizeBefore+1, Model.activeUser.getEntries().size());
		assertEquals(6.5, Model.activeUser.getSumOfWorkTime(), 0.000001);
		assertEquals(-1.5, Model.activeUser.getWorkTimeStatus(), 0.00001);
		assertEquals(1, Model.activeUser.getDaysWorked());
	}
	
	/**
	 * Tests model.User.addEntryOverwrite() function.
	 * Tests the effect of addOverwrite method in User class on user's properties 
	 * when an (1) entry without an end time, (2) a complete entry, (3) an absence 
	 * entry and (4) lastly again the same entry as in (2) are added in 
	 * sequence.
	 * @throws InterruptedException 
	 */
	@Test
	public void testAddOverwriteUserProperties() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		int sizeBefore = Model.activeUser.getEntries().size();
		
		Model.activeUser.addEntryOverwrite(new model.TimeEntry(LocalDate.now(), 
				LocalTime.of(8, 0, 0), null));
		Model.activeUser.addEntryOverwrite(new model.TimeEntry(LocalDate.now(), 
				LocalTime.of(8, 0, 0), LocalTime.of(14, 0, 0)));
		Model.activeUser.addEntryOverwrite(new model.AbsenceEntry(LocalDate.now(), 
				"Urlaub"));
		Model.activeUser.addEntryOverwrite(new model.TimeEntry(LocalDate.now(), 
				LocalTime.of(8, 0, 0), LocalTime.of(14, 0, 0)));
		
		assertEquals(sizeBefore+1, Model.activeUser.getEntries().size());
		assertEquals(5.25, Model.activeUser.getSumOfWorkTime(), 0.000001);
		assertEquals(-2.75, Model.activeUser.getWorkTimeStatus(), 0.00001);
		assertEquals(1, Model.activeUser.getDaysWorked());
	}
	
	/**
	 * Tests model.User.addEntryNoStats() function.
	 */
	@Test
	public void testAddEntryNoStats() {
		Model.activeUser = Model.users.get("admin");
		int sizeBefore = Model.activeUser.getEntries().size();
		double sumBefore = Model.activeUser.getSumOfWorkTime();
		double statusBefore  = Model.activeUser.getWorkTimeStatus();
		int daysBefore = Model.activeUser.getDaysWorked();
		
		Model.activeUser.addEntryNoStats(new model.TimeEntry(LocalDate.now(), 
				LocalTime.of(8, 0, 0), LocalTime.of(14, 0, 0)));
		Model.activeUser.addEntryNoStats(new model.AbsenceEntry(LocalDate.now(), 
				"Urlaub"));
		
		assertEquals(sizeBefore+1, Model.activeUser.getEntries().size());
		assertEquals(sumBefore, Model.activeUser.getSumOfWorkTime(), 0.000001);
		assertEquals(statusBefore, Model.activeUser.getWorkTimeStatus(), 0.00001);
		assertEquals(daysBefore, Model.activeUser.getDaysWorked());
	}
	
	/**
	 * Tests model.User.removeEntryOnDate() function.
	 * Tests removing an existing entry in user data in Model.
	 * @throws InterruptedException 
	 */
	@Test
	public void testRemoveEntryOnDate() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		int sizeBefore = Model.activeUser.getEntries().size();
		testAddEntryModel();
		Model.activeUser.removeEntryOnDate(LocalDate.now());
		assertEquals(sizeBefore, Model.activeUser.getEntries().size());
	}
	
	/**
	 * Tests model.User.calculateStatistics() function.
	 */
	@Test
	public void testCalculateStatistics() {
		Model.activeUser = Model.users.get("mitarbeiter");
		Model.activeUser.addEntry(new TimeEntry(LocalDate.of(2018, Month.JUNE, 5), 
				LocalTime.of(07, 15, 00), 
				LocalTime.of(16, 15, 00)));
		Model.activeUser.addEntry(new TimeEntry(LocalDate.of(2018, Month.JUNE, 6), 
				LocalTime.of(07, 00, 00), 
				LocalTime.of(19, 00, 00)));
		Model.activeUser.addEntry(new AbsenceEntry(LocalDate.of(2018, Month.JUNE, 7), 
				"Krankheit"));
		Model.activeUser.addEntry(new TimeEntry(LocalDate.of(2018, Month.JUNE, 8), 
				LocalTime.of(16, 00, 00), 
				LocalTime.of(19, 00, 00)));
		
		double stats[] = Model.activeUser.calculateStatistics(
				LocalDate.of(2018, Month.JUNE, 3), LocalDate.of(2018, Month.JUNE, 17));
		
		// sumOfWorkTime
		assertEquals(8.25 + 11.25 + 3, stats[0], 0.0001);
		// average time worked per day
		assertEquals((8.25 + 11.25 + 3) / 3, stats[1], 0.0001);
		// work time status (whether the user has overtime or undertime)
		assertEquals(0.25 + 3.25 - 5, stats[2], 0.0001);
		
	}
	
	
	// Tests related to the Statistics window
	
	/**
	 * Tests showing the user statistics.
	 * 
	 * @throws InterruptedException
	 * @throws ParseException 
	 */
	@Test
	public void testStatisticsOkButton() throws InterruptedException, ParseException {
		testCalculateStatistics();
		
		double stats[] = Model.activeUser.calculateStatistics(
				LocalDate.of(2018, Month.JUNE, 3), LocalDate.of(2018, Month.JUNE, 17));

		setUpMitarbeiterUser();
		
		StartWindow startWin = (StartWindow) Main.activeWindowObject;
		startWin.statisticsButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.STATISTIKEN, Main.activeWindow);
		Statistics statisticsWin = 
				(Statistics) Main.activeWindowObject;
		statisticsWin.intervalStartPicker.setDate(
				Utilities.dateFormat.parse("4/6/2018"));
		statisticsWin.intervalEndPicker.setDate(
				Utilities.dateFormat.parse("20/6/2018"));
		statisticsWin.okButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.STATISTIKEN, Main.activeWindow);
		
		NumberFormat format = NumberFormat.getInstance(Locale.GERMANY);
		Number tmpRepres = format.parse(statisticsWin.sumOfWorkTimeStat
		                                             .getText()
		                                             .strip());
		double sum = tmpRepres.doubleValue();
		tmpRepres = format.parse(statisticsWin.workTimePerDayStat
		                                      .getText()
		                                      .strip());
		double avgTime = tmpRepres.doubleValue();
		tmpRepres = format.parse(statisticsWin.workTimeStatusStat
		                                      .getText()
		                                      .strip());
		double status = tmpRepres.doubleValue();
		// Asserts that showed data is the same as the data contained 
		// in the model
		assertEquals(sum, stats[0], 0.001);
		assertEquals(avgTime, stats[1], 0.001);
		assertEquals(status, stats[2], 0.001);
		statisticsWin.frame.dispose();		
	}
	
	/**
	 * Tests returning from the Statistics window to the start window.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testStatisticsBackButton() throws InterruptedException {
		setUpMitarbeiterUser();
		
		StartWindow startWin = (StartWindow) Main.activeWindowObject;
		startWin.statisticsButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.STATISTIKEN, Main.activeWindow);
		Statistics statisticsWin = 
				(Statistics) Main.activeWindowObject;
		statisticsWin.backButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
	// Tests adding and deleting an entry
	
	/**
	 * Tests adding an entry. 
	 * 
	 * @throws InterruptedException 
	 */
	@Test
	public void testAddEntry() throws InterruptedException {
		User mitarbeiter = Model.users.get("mitarbeiter");
		int sizeBefore = mitarbeiter.getEntries().size();
		setUpMitarbeiterUser();
		
		StartWindow startWin = (StartWindow) Main.activeWindowObject;
		startWin.startEndButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.STARTENDTIMEDOCUMENTED, Main.activeWindow);
		StartEndTimeDocumented documentedWin = 
				(StartEndTimeDocumented) Main.activeWindowObject;
		documentedWin.returnToLoginButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		((Authentication)Main.activeWindowObject).frame.dispose();
		
		setUpMitarbeiterUser();
		startWin = (StartWindow) Main.activeWindowObject;
		startWin.startEndButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.STARTENDTIMEDOCUMENTED, Main.activeWindow);
		documentedWin = (StartEndTimeDocumented) Main.activeWindowObject;
		documentedWin.returnToLoginButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		assertEquals(sizeBefore + 1, mitarbeiter.getEntries().size());
		// Date today
		LocalDate today = LocalDate.now();
		assertNotEquals(null, 
					 mitarbeiter.getEntryOnDate(today));
		((Authentication)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests deleting of an entry. Mitarbeiter adds an entry and then admin
	 * deletes it.
	 * 
	 * Expected behavior: Entry is added and then deleted from the mitarbeiter 
	 * user's data.
	 * Tests DataWindow as well.
	 * 
	 * @throws InterruptedException
	 * @throws ParseException 
	 */
	@Test
	public void deleteEntry() throws InterruptedException, ParseException {
		User mitarbeiter = Model.users.get("mitarbeiter");
		mitarbeiter.addEntry(new AbsenceEntry(LocalDate.now(), "Urlaub"));
		mitarbeiter.addEntry(new TimeEntry(LocalDate.now().plusDays(1), 
				LocalTime.of(10, 20, 50),
				LocalTime.of(18,  20, 30)));
		int sizeBefore = Model.users.get("mitarbeiter").getEntries().size();

		// Log in as admin
		setUpAdminUser();
		// Open user administration window
		StartWindow startWin = (StartWindow) Main.activeWindowObject;
		startWin.userAdminButton.doClick();
		Thread.sleep(threadSleepTime);
		// Open a window that allows the admin to delete user's data
		assertEquals(Windows.USER_ADMIN, Main.activeWindow);
		UserAdminWindow userAdminWin = (UserAdminWindow) Main.activeWindowObject;
		userAdminWin.modifyUserDataButton.doClick();
		Thread.sleep(threadSleepTime);
		// Show mitarbeiter user's data
		assertEquals(Windows.DATA_WINDOW, Main.activeWindow);
		DataWindow dataWin = (DataWindow) Main.activeWindowObject;
		dataWin.chooseUserField.setText("mitarbeiter");
		dataWin.chooseUserButton.doClick();
		Thread.sleep(threadSleepTime);
		dataWin.table.setRowSelectionInterval(0, 0);
		
		dataWin.deleteEntryButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(sizeBefore - 1, Model.users.get("mitarbeiter").getEntries().size());
		assertEquals(Windows.DATA_WINDOW, Main.activeWindow);
		dataWin.frame.dispose();
	}
		
	/**
	 * Tests how the system reacts when we add a Fehlzeitgrund on dates where it 
	 * overlaps with the dates where the user was at work.
	 * 
	 * Expected behavior: Entries for the Fehlzeitgrund are added to the user's data.
	 * On dates where work and absence overlap work takes precedence.
	 * 
	 * @throws InterruptedException
	 * @throws ParseException 
	 */
	@Test
	public void testFehlzeitgrundOverWorkEntries() throws InterruptedException, ParseException {
		setUpAdminUser();
		// Set up
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		StartWindow startWin = (StartWindow) Main.activeWindowObject;
		startWin.fehlzeitButton.doClick();
		Thread.sleep(threadSleepTime);
		
		// Insert start and end time for some date
		Model.activeUser.addEntryOverwrite(new model.TimeEntry(LocalDate.of(2018, Month.JUNE, 17), 
				LocalTime.of(8, 0, 0), LocalTime.of(14, 0, 0)));
		
		
		// Insert Fehlzeitgrund
		assertEquals(Windows.FEHLZEITGRUND_EINGABE, Main.activeWindow);
		FehlzeitgrundEingabe fehlzeitWin = (FehlzeitgrundEingabe) Main.activeWindowObject;
		fehlzeitWin.startDatePicker.setDate(Utilities.dateFormat.parse("16/06/2018"));
		fehlzeitWin.endDatePicker.setDate(Utilities.dateFormat.parse("18/06/2018"));
		fehlzeitWin.grundList.setSelectedIndex(0);
		Thread.sleep(threadSleepTime);
		fehlzeitWin.okButton.doClick();
		Thread.sleep(threadSleepTime);
		
		Map<LocalDate, model.Entry> userData = Model.activeUser.getEntries();
		model.AbsenceEntry entry1 = (model.AbsenceEntry)userData.get(LocalDate.of(2018, Month.JUNE, 16));
		model.TimeEntry entry2 = (model.TimeEntry)userData.get(LocalDate.of(2018, Month.JUNE, 17));
		model.AbsenceEntry entry3 = (model.AbsenceEntry)userData.get(LocalDate.of(2018, Month.JUNE, 18));
		assertEquals("Urlaub", entry1.getAbsenceReason());
		assertEquals(LocalTime.of(8, 0, 0), entry2.getStartTime());
		assertEquals(LocalTime.of(14, 0, 0), entry2.getEndTime());
		assertEquals("Urlaub", entry3.getAbsenceReason());
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
	// Tests related to authentication
	
	/**
	 * Tests whether the admin user when authenticated gets the admin start page
	 * i.e. whether s/he gets access to the functions that correspond to its authorization
	 * level 
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testAuthenticateAdmin() throws InterruptedException {
		auth = new Authentication();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		auth.usernameField.setText("admin");
		auth.passwordField.setText("admin");
		auth.loginButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}

	/**
	 * Tests whether the mitarbeiter user when authenticated gets the mitarbeiter start page
	 * i.e. whether s/he gets access to the functions that correspond to its authorization
	 * level 
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testAuthenticateMitarbeiter() throws InterruptedException {
		auth = new Authentication();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		auth.usernameField.setText("mitarbeiter");
		auth.passwordField.setText("mitarbeiter");
		auth.loginButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests the behavior of the system when the user enters a false username.
	 * 
	 * Expected behavior: User stays on the authentication window. False username
	 * of password label is shown.
	 * @throws InterruptedException
	 */
	@Test
	public void testFalseUsername() throws InterruptedException {
		auth = new Authentication();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		auth.usernameField.setText("falseUsername");
		auth.passwordField.setText("mitarbeiter");
		auth.loginButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		assertEquals(null, Model.activeUser);
		auth.frame.dispose();
	}
	
	/**
	 * Tests the behavior of the system when the user enters a false password.
	 * 
	 * Expected behavior: User stays on the authentication window. False username
	 * of password label is shown.
	 * @throws InterruptedException
	 */
	@Test
	public void testFalsePassword() throws InterruptedException {
		auth = new Authentication();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		auth.usernameField.setText("mitarbeiter");
		auth.passwordField.setText("falsePassword");
		auth.loginButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		assertEquals(null, Model.activeUser);
		auth.frame.dispose();
	}
	
	
	// Tests related to user window
	
	/**
	 * Tests the behavior of the system when the user logs off.
	 * 
	 * Expected behavior: Authentication window is shown to the user 
	 * @throws InterruptedException
	 */
	@Test
	public void testLoggingOff() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		StartWindow adminWin = new StartWindow();
		Main.activeWindowObject = adminWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		adminWin.logoffButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		((Authentication)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests the behavior of the system when the user clicks on MeineDaten button.
	 * 
	 * Expected behavior: MeineDaten window is shown to the user 
	 * @throws InterruptedException
	 */
	@Test
	public void testDataWindow() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		StartWindow adminWin = new StartWindow();
		Main.activeWindowObject = adminWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		adminWin.dataButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.DATA_WINDOW, Main.activeWindow);
		((DataWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
	/**
	 * Tests the behavior of the system when the user clicks on 'Fehlzeitgrund eingeben' button.
	 * 
	 * Expected behavior: Fehlzeitgrundeingabe window is shown to the user 
	 * @throws InterruptedException
	 */
	@Test
	public void testFehlzeitgrundeingabe() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		StartWindow adminWin = new StartWindow();
		Main.activeWindowObject = adminWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		adminWin.fehlzeitButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.FEHLZEITGRUND_EINGABE, Main.activeWindow);
		((FehlzeitgrundEingabe)Main.activeWindowObject).frame.dispose();
	}
	
	
	// Testing of the start and end time documentation.
	
	/**
	 * Tests the behavior of the system when the user documents the start time.
	 * 
	 * Expected behavior: StartEndTimeDocumented window is shown to the user and 
	 * the boolean that stands for the user being at work is set.
	 * 
	 *  
	 * @throws InterruptedException
	 */
	@Test
	public void testKommzeitDokumentieren() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		StartWindow adminWin = new StartWindow();
		Main.activeWindowObject = adminWin;
		Thread.sleep(threadSleepTime);
		int sizeBefore = Model.activeUser.getEntries().size();
		adminWin.startEndButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(true, Model.activeUser.isAtWork());
		assertEquals(sizeBefore+1, Model.activeUser.getEntries().size());
		assertEquals(Windows.STARTENDTIMEDOCUMENTED, Main.activeWindow);
		assertEquals("admin", Model.activeUser.getName());
		((StartEndTimeDocumented)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests the behavior of the system when the user documents Komm- und Gehzeit.
	 * 
	 * Expected behavior: Same as the previous test + then the next time the user 
	 * logs in, the label 'Gehzeit dokumentieren' should be shown. Furthermore, when 
	 * the user then documents the end time and clicks on the 'Zurueck zum Login', 
	 * boolean that corresponds to the user being at work should be set to False
	 * and the authentication window should again be shown.
	 *  
	 * @throws InterruptedException
	 */
	@Test
	public void testGehzeitDokumentieren() throws InterruptedException {
		
		testKommzeitDokumentieren();
		
		StartEndTimeDocumented startEndWin = (StartEndTimeDocumented) Main.activeWindowObject;
		startEndWin.returnToLoginButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		((Authentication)Main.activeWindowObject).frame.dispose();
		
		// Documenting end time
		Model.activeUser = Model.users.get("admin");
		StartWindow adminWinEndTime = new StartWindow();
		Main.activeWindowObject = adminWinEndTime;
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		int sizeAfterKommzeit = Model.activeUser.getEntries().size();
		assertEquals("Gehzeit dokumentieren", adminWinEndTime.startEndButton.getText());
		adminWinEndTime.startEndButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(false, Model.activeUser.isAtWork());
		assertEquals(sizeAfterKommzeit, Model.activeUser.getEntries().size());
		assertEquals(Windows.STARTENDTIMEDOCUMENTED, Main.activeWindow);
		assertEquals("admin", Model.activeUser.getName());
		StartEndTimeDocumented startEndWinEndTime = (StartEndTimeDocumented) Main.activeWindowObject;
		startEndWinEndTime.returnToLoginButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.AUTHENTICATION, Main.activeWindow);
		assertEquals(null, Model.activeUser);		
		((Authentication)Main.activeWindowObject).frame.dispose();
	}
	
	
	// Tests related to the Fehlzeitgrundeingabe window
		
	/**
	 * Tests returning to the start window from Fehlzeitgrundeingabe window.
	 * 
	 * Expected behavior: User returns to the start page (this page contains
	 * functions that correspond to hers/his Rolle) without changing anything.
	 *  
	 * Here we will also have to confirm that nothing was added to the workers data
	 * (For the time being we assume that the data is an ArrayList of some objects 
	 * that represent entries in it)
	 *  
	 * @throws InterruptedException
	 */
	@Test
	public void testFehlzeiteingabeBack() throws InterruptedException {
		// Setting up
		Model.activeUser = Model.users.get("mitarbeiter");
		FehlzeitgrundEingabe fehlzeitWin = new FehlzeitgrundEingabe();
		Main.activeWindowObject = fehlzeitWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.FEHLZEITGRUND_EINGABE, Main.activeWindow);
		
		fehlzeitWin.backButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests adding of the new Fehlzeitgrund.
	 * 
	 * Expected behavior: User returns to the start page (this page contains
	 * functions that correspond to hers/his Rolle), Fehlzeitgrund is added 
	 * to hers/his data.
	 *  
	 * Here we will also have to check whether something was added to the workers data
	 * (For the time being we assume that the data is an arraylist of some objects 
	 * that represent entries in it)
	 *  
	 * @throws InterruptedException
	 * @throws ParseException 
	 */
	@Test
	public void testFehlzeiteingabeConfirm() throws InterruptedException, ParseException {
		// Setting up
		Model.activeUser = Model.users.get("mitarbeiter");
		FehlzeitgrundEingabe fehlzeitWin = new FehlzeitgrundEingabe();
		Main.activeWindowObject = fehlzeitWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.FEHLZEITGRUND_EINGABE, Main.activeWindow);
		
		fehlzeitWin.startDatePicker.setDate(Utilities.dateFormat.parse("20/6/2018"));
		fehlzeitWin.endDatePicker.setDate(Utilities.dateFormat.parse("22/6/2018"));
		
		fehlzeitWin.okButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		assertEquals(3, Model.users.get("mitarbeiter").getEntries().size());
		assertNotEquals(null, Model.users.get("mitarbeiter").getEntryOnDate(
				LocalDate.of(2018, Month.JUNE, 20)));
		assertNotEquals(null, Model.users.get("mitarbeiter").getEntryOnDate(
				LocalDate.of(2018, Month.JUNE, 21)));
		assertNotEquals(null, Model.users.get("mitarbeiter").getEntryOnDate(
				LocalDate.of(2018, Month.JUNE, 22)));
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
	// Tests related to the LateInput window
		
	/**
	 * Tests returning to the start window from LateInput window.
	 * 
	 * Expected behavior: User returns to the start page (this page contains
	 * functions that correspond to hers/his Rolle), nothing is changed in the system.
	 *  
	 * Here we will also have to confirm that nothing was added to the workers data
	 * (For the time being we assume that the data is an ArrayList of some objects 
	 * that represent entries in it. In addition, we assume that in some class Model 
	 * there is a HashMap usersData that contains user data)
	 *  
	 * @throws InterruptedException
	 */
	@Test
	public void testLateInputBack() throws InterruptedException {
		// Setting up
		Model.activeUser = Model.users.get("admin");
		LateInput lateInputWin = new LateInput();
		Main.activeWindowObject = lateInputWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.LATE_INPUT, Main.activeWindow);
		
		int sizeBefore = Model.activeUser.getEntries().size();
				
		lateInputWin.backButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.USER_ADMIN, Main.activeWindow);
		assertEquals(sizeBefore, Model.activeUser.getEntries().size());
		((UserAdminWindow)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests late input of working hours for an existing user.
	 * @throws InterruptedException 
	 * @throws ParseException 
	 */
	@Test
	public void testLateInputConfirm() throws InterruptedException, ParseException {
		// Setting up
		Model.activeUser = Model.users.get("admin");
		LateInput lateInputWin = new LateInput();
		Main.activeWindowObject = lateInputWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.LATE_INPUT, Main.activeWindow);
		
		int numEntriesBefore = Model.users.get("mitarbeiter").getEntries().size();
		lateInputWin.usernameField.setText("mitarbeiter");
		lateInputWin.datumPicker.setDate(
				Utilities.dateFormat.parse("20/6/2018"));

		lateInputWin.confirmButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.USER_ADMIN, Main.activeWindow);
		assertEquals(numEntriesBefore + 1, 
				Model.users.get("mitarbeiter").getEntries().size());
		assertNotEquals(null, Model.users.get("mitarbeiter").getEntryOnDate(
				LocalDate.of(2018, Month.JUNE, 20)));
		((UserAdminWindow)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests late input of the working hours for the user that does not exist.
	 * 
	 * Expected behavior: User stays on the late input window and nothing is added
	 * to the database.
	 *  
	 * Here we will also have to confirm that nothing was added to the workers data
	 * (For the time being we assume that the data is an ArrayList of some objects 
	 * that represent entries in it. In addition, we assume that in some class Model 
	 * there is a HashMap usersData that contains user data)
	 *  
	 * @throws InterruptedException
	 */
	@Test
	public void testLateInputConfirmNoUser() throws InterruptedException {
		// Setting up
		Model.activeUser = Model.users.get("admin");
		LateInput lateInputWin = new LateInput();
		Main.activeWindowObject = lateInputWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.LATE_INPUT, Main.activeWindow);
		
		int numEntriesBefore = Model.activeUser.getEntries().size();
		lateInputWin.usernameField.setText("John");
		
		lateInputWin.confirmButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.LATE_INPUT, Main.activeWindow);
		assertEquals(numEntriesBefore, Model.activeUser.getEntries().size());
		((LateInput)Main.activeWindowObject).frame.dispose();
	}
		
	
	// Tests related to the DataWindow
	
	/**
	 * Tests returning to the start window from MeineDaten window.
	 * 
	 * Expected behavior: User returns to the start page (this page contains
	 * functions that correspond to hers/his Rolle), nothing is changed in the system.
	 *  
	 * @throws InterruptedException
	 */
	@Test
	public void testDataWindowBack() throws InterruptedException {
		// Setting up
		Model.activeUser = Model.users.get("mitarbeiter");
		Model.activeUser.addEntryOverwrite(new TimeEntry(LocalDate.now(), 
				LocalTime.now(),
				LocalTime.now()));
		DataWindow meineDatenWin = new DataWindow();
		Main.activeWindowObject = meineDatenWin;
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.DATA_WINDOW, Main.activeWindow);
		
		meineDatenWin.backButton.doClick();
		Thread.sleep(threadSleepTime);
		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
	// Tests related to the AddUser window
	
	/**
	 * Tests adding a new user to the system.
	 * @throws InterruptedException
	 */
	@Test
	public void testAddUserOk() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		AddUser addUserWin = new AddUser();
		Main.activeWindowObject = addUserWin;
		Thread.sleep(threadSleepTime);

		int numOfUsersBefore = Model.users.size();
				
		addUserWin.userNameField.setText("Hans");
		addUserWin.okButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.USER_ADMIN, Main.activeWindow);
		assertEquals(numOfUsersBefore + 1, Model.users.size());
		assertNotEquals(null, Model.users.get("Hans"));
		((UserAdminWindow)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests returning from the AddUser window to the user administration window.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testAddUserBack() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		AddUser addUserWin = new AddUser();
		Main.activeWindowObject = addUserWin;
		
		Thread.sleep(threadSleepTime);
		int numOfUsersBefore = Model.users.size();
				
		addUserWin.userNameField.setText("Hans");
		addUserWin.backButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.USER_ADMIN, Main.activeWindow);
		assertEquals(numOfUsersBefore, Model.users.size());
		((UserAdminWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
	// Tests related to the DeleteUser window
	
	/**
	 * Tests deleting a user from the system.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testDeleteUserDelete() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		DeleteUser deleteUserWin = new DeleteUser();
		Main.activeWindowObject = deleteUserWin;
		
		Thread.sleep(threadSleepTime);
		int numOfUsersBefore = Model.users.size();
			
		// User 'mitarbeiter' is in the first row because it was added first
		deleteUserWin.table.setRowSelectionInterval(0, 0);
		deleteUserWin.deleteUserButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.DELETE_USER, Main.activeWindow);
		assertEquals(numOfUsersBefore - 1, Model.users.size());
		assertEquals(null, Model.users.get("mitarbeiter"));
		deleteUserWin.frame.dispose();
	}
	
	/**
	 * Tests returning from the DeleteUser window to the user 
	 * administration window.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testDeleteUserBack() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		DeleteUser deleteUserWin = new DeleteUser();
		Main.activeWindowObject = deleteUserWin;
		Thread.sleep(threadSleepTime);
		
		int numOfUsersBefore = Model.users.size();
			
		deleteUserWin.backButton.doClick();
		Thread.sleep(threadSleepTime);
		
		assertEquals(Windows.USER_ADMIN, Main.activeWindow);
		assertEquals(numOfUsersBefore, Model.users.size());
		((UserAdminWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
	
	// Tests related to the RoleAdmin window
	
	/**
	 * Tests adding a new role to the system.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testRoleAdminAddRole() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		Main.activeWindowObject = new RoleAdmin();
		int numOfRolesBefore = Model.roles.size();
		
		System.out.println(Model.roles.size());
		
		
		RoleAdmin roleAdminWin = (RoleAdmin) Main.activeWindowObject;
		Thread.sleep(threadSleepTime);
		System.out.println(Utilities.rolesTable.size());
		
		roleAdminWin.addRoleField.setText("Geschäftsführer");
		Thread.sleep(threadSleepTime);
		roleAdminWin.addRoleButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(Windows.ROLE_ADMIN, Main.activeWindow);
		assertEquals(numOfRolesBefore + 1, Model.roles.size());
		assertNotEquals(null, Model.roles.get("Geschäftsführer"));
		roleAdminWin.frame.dispose();
	}
	
	/**
	 * Tests deleting a role A when there are no users that are in role A.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testRoleAdminDeleteRoleWithoutUsers() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		Main.activeWindowObject = new RoleAdmin();
		
		Model.addRole("RoleWithNoUsers", new Role("RoleWithNoUsers"));
		int numOfRolesBefore = Model.roles.size();
		
		RoleAdmin roleAdminWin = (RoleAdmin) Main.activeWindowObject;
		Thread.sleep(threadSleepTime);
		
		// Role 'RoleWithNoUsers' is in the third row because it was added last
		roleAdminWin.table.setRowSelectionInterval(2, 2);
		Thread.sleep(threadSleepTime);
		roleAdminWin.deleteRoleButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(Windows.ROLE_ADMIN, Main.activeWindow);
		assertEquals(numOfRolesBefore - 1, Model.roles.size());
		assertEquals(null, Model.roles.get("RoleWithNoUsers"));
		roleAdminWin.frame.dispose();
	}

	/**
	 * Tests deleting a role A when there exists a user in role A.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testRoleAdminDeleteRoleWithUsers() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		Main.activeWindowObject = new RoleAdmin();
		int numOfRolesBefore = Model.roles.size();
		
		RoleAdmin roleAdminWin = (RoleAdmin) Main.activeWindowObject;
		Thread.sleep(threadSleepTime);
		
		// Role 'Mitarbeiter' is in the first row because it was added first
		roleAdminWin.table.setRowSelectionInterval(0, 0);
		roleAdminWin.deleteRoleButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(Windows.ROLE_ADMIN, Main.activeWindow);
		assertEquals(numOfRolesBefore, Model.roles.size());
		assertNotEquals(null, Model.roles.get("Mitarbeiter"));
	}
	
	/**
	 * Tests returning from the RoleAdmin window to the start window.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testRoleAdminBack() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		Main.activeWindowObject = new RoleAdmin();
		int numOfRolesBefore = Model.roles.size();
		
		RoleAdmin roleAdminWin = (RoleAdmin) Main.activeWindowObject;
		Thread.sleep(threadSleepTime);
		
		roleAdminWin.backButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		assertEquals(numOfRolesBefore, Model.roles.size());
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
	// Tests related to the SystemCustomization window
	
	/**
	 * Tests changing the flexibility limits in the system.
	 * 
	 * @throws InterruptedException
	 * @throws ParseException
	 */
	@Test
	public void testSystemCustomizationChangeFlexi() throws InterruptedException, ParseException {
		Model.activeUser = Model.users.get("admin");
		Main.activeWindowObject = new SystemCustomization();
		
		LocalTime endFlexiBefore[] =  Model.endFlexibility;
		LocalTime pauseBefore[] =  Model.pauseStartEnd;
		
		SystemCustomization sysCustomWin = (SystemCustomization) Main.activeWindowObject;
		Thread.sleep(threadSleepTime);
		
		sysCustomWin.flexibilityList.setSelectedItem("Kommzeit");
		sysCustomWin.lowerLimitSpinner.setValue(Utilities.timeFormat.parse("10:00:00"));
		sysCustomWin.upperLimitSpinner.setValue(Utilities.timeFormat.parse("12:00:00"));
		sysCustomWin.changeFlexiButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(Windows.SYSTEM_CUSTOMIZATION, Main.activeWindow);
		assertEquals(LocalTime.of(10, 0, 0), Model.startFlexibility[0]);
		assertEquals(LocalTime.of(12, 0, 0), Model.startFlexibility[1]);
		assertArrayEquals(endFlexiBefore, Model.endFlexibility);
		assertArrayEquals(pauseBefore, Model.pauseStartEnd);
		((SystemCustomization)Main.activeWindowObject).frame.dispose();
	}
	
	/**
	 * Tests a new absence reason to the system.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testSystemCustomizationAddReason() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		Main.activeWindowObject = new SystemCustomization();
		
		int numOfReasonsBefore = Model.absenceReasons.size();
		
		SystemCustomization sysCustomWin = (SystemCustomization) Main.activeWindowObject;
		Thread.sleep(threadSleepTime);

		sysCustomWin.addReasonField.setText("Geschäftsreise");
		sysCustomWin.addReasonButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(Windows.SYSTEM_CUSTOMIZATION, Main.activeWindow);
		assertEquals(numOfReasonsBefore + 1, Model.absenceReasons.size());
		assertTrue(Model.absenceReasons.contains("Geschäftsreise"));
		sysCustomWin.frame.dispose();
	}
	
	/**
	 * Tests deleting an existing absence reason from the system.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testSystemCustomizationDeleteReason() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		Main.activeWindowObject = new SystemCustomization();
		
		int numOfReasonsBefore = Model.absenceReasons.size();
		
		SystemCustomization sysCustomWin = (SystemCustomization) Main.activeWindowObject;
		Thread.sleep(threadSleepTime);
		
		sysCustomWin.deleteReasonList.setSelectedItem("Urlaub");
		sysCustomWin.deleteReasonButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(Windows.SYSTEM_CUSTOMIZATION, Main.activeWindow);
		assertEquals(numOfReasonsBefore - 1, Model.absenceReasons.size());
		assertFalse(Model.absenceReasons.contains("Urlaub"));
		sysCustomWin.frame.dispose();
	}
	
	/**
	 * Tests returning from the SystemCustomization window to the start window. 
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testSystemCustomizationBack() throws InterruptedException {
		Model.activeUser = Model.users.get("admin");
		Main.activeWindowObject = new SystemCustomization();
		
		int numOfReasonsBefore = Model.absenceReasons.size();
		LocalTime startFlexiBefore[] =  Model.startFlexibility;
		LocalTime endFlexiBefore[] =  Model.endFlexibility;
		LocalTime pauseBefore[] =  Model.pauseStartEnd;
		
		SystemCustomization sysCustomWin = (SystemCustomization) Main.activeWindowObject;
		Thread.sleep(threadSleepTime);
		
		sysCustomWin.backButton.doClick();
		Thread.sleep(threadSleepTime);

		assertEquals(Windows.START_WINDOW, Main.activeWindow);
		assertEquals(numOfReasonsBefore, Model.absenceReasons.size());
		assertArrayEquals(startFlexiBefore, Model.startFlexibility);
		assertArrayEquals(endFlexiBefore, Model.endFlexibility);
		assertArrayEquals(pauseBefore, Model.pauseStartEnd);
		((StartWindow)Main.activeWindowObject).frame.dispose();
	}
	
	
}
