package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.Model;
import model.Role;
import model.User;
import net.miginfocom.swing.MigLayout;

/**
 * Window that allows the user to add new user.
 */
public class AddUser {

	// Attributes that were moved so that we can use them in tests
	JTextField userNameField;
	JPasswordField passwordField;
	
	String comboBoxList[];
	{
		comboBoxList = Model.roles.keySet().toArray(new String[0]);
	}
	JComboBox<String> roleList;
	JSpinner hoursPerWeek;
	
	JButton okButton;
	JButton backButton;
	
	JFrame frame;
	
	JLabel falseEntryMsg;
	boolean falseEntryMsgDrawn = false;

	public AddUser() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.ADD_USER;

				// Create Frame
				frame = new JFrame("Benutzer hinzufügen");
				
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// Layout Window
				frame.setLayout(new MigLayout("align 50% 50%, insets 20"));

				// Objects of the Frame
				JLabel userNameLabel = new JLabel("Benutzername");
				JLabel passwordLabel = new JLabel("Password");
								
				userNameField = new JTextField(10);
								
				passwordField = new JPasswordField(10);
				
				JLabel role = new JLabel("Rolle");
				
			    roleList = new JComboBox<String>(comboBoxList);
			    
			    JLabel hoursWeekLabel = new JLabel("Wochenstunden");			    
			    SpinnerNumberModel numModel = new SpinnerNumberModel(40, 0, 168, 1);
			    hoursPerWeek = new JSpinner(numModel);
			    
				okButton = new JButton("Benutzer hinzufügen");
				backButton = new JButton("Zurück");
			
				
				frame.add(userNameLabel, "cell 0 0");
				frame.add(userNameField, "cell 0 1");
				
				frame.add(passwordLabel, "cell 2 0");
				
				frame.add(passwordField, "cell 2 1");
				
				frame.add(role, "cell 4 0");
				
				frame.add(roleList, "cell 4 1");
				
				frame.add(hoursWeekLabel, "cell 5 0");
				frame.add(hoursPerWeek, "cell 5 1");
				
				frame.add(okButton, "cell 2 5, span");
				
				frame.add(backButton, "cell 0 5");
				

				//Window Settings
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);
				
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						// Add the user with specified properties
						String nameStr = userNameField.getText();
						String passStr = new String(passwordField.getPassword());
						String chosenRoleStr = roleList.getSelectedItem().toString();
						Role chosenRole = Model.roles.get(chosenRoleStr);
						int weekHours = (int) hoursPerWeek.getValue();
						
						User userToAdd = new User(nameStr, passStr, chosenRole, weekHours);
						Model.addUser(nameStr, userToAdd);
						
						frame.setVisible(false);
						Main.activeWindowObject = new UserAdminWindow();						
					}
						
				});
				
				backButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new UserAdminWindow();
						
					}
				});
				

			}
		});
	}
}