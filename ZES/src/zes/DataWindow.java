package zes;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import model.Model;
import model.Utilities;
import net.miginfocom.swing.MigLayout;

/**
 * Window that shows the data related to the user or to the chosen user.
 */
public class DataWindow {

	// Attributes that were moved so that we can use them in tests
	JButton backButton;
	JButton deleteEntryButton;
	JLabel dataChosenUser = new JLabel("");
	JLabel infoMsg = new JLabel("");
	
	JButton chooseUserButton;
	JTextField chooseUserField;
	JTable table;
	
	JFrame frame;
	
	private Vector<Vector<String>> dataForModel = new Vector<>();
	private ArrayList<Boolean> notes = new ArrayList<>();
	
	public DataWindow() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.DATA_WINDOW;
				
				// Create Frame
				frame = new JFrame("Daten");

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				// set layout
				frame.setLayout(new MigLayout("align 50% 50%"));

				// Window Settings
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);
				
				// Titles of the columns
				Vector<String> title = new Vector<>(Arrays.asList("Tag", "Anfangszeit", "Endzeit"));
	
				// Choose data for the model depending on whether we show
				// data of the active or chosen user
				if (Model.ownData) {
					fillDataForModel(Model.activeUser, dataForModel, notes);
				}
				
				// Table model with cells that are not editable
				DefaultTableModel tableModel = new DefaultTableModel(dataForModel, title) {
					
					private static final long serialVersionUID = 1L;

					@Override
				    public boolean isCellEditable(int row, int column) {
				        return false;
				    }
				};
								
				table = new JTable(tableModel) {
				
					private static final long serialVersionUID = 1L;

					public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
					{
						Component comp = super.prepareRenderer(renderer, row, column);

						// If there is a note for this entry, set background to red
						if (!isRowSelected(row))
						{
							comp.setBackground(getBackground());
							int modelRow = convertRowIndexToModel(row);
													
							if (notes.size() > modelRow) {
								if (notes.get(modelRow))
									comp.setBackground(Color.RED);
							}
						}

						return comp;
					}
				};
				// Allow selection of only one row
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				
				backButton = new JButton("Zurück");
				backButton.setPreferredSize(new Dimension(150, 40));
				
				deleteEntryButton = new JButton("Eintrag löschen");
				deleteEntryButton.setPreferredSize(new Dimension(150, 40));
				
				chooseUserButton = new JButton("Benutzer auswählen	");
				chooseUserButton.setPreferredSize(new Dimension(150, 40));
				
				chooseUserField = new JTextField(15);
				chooseUserField.setPreferredSize(new Dimension(150, 40));
				
				if (Model.chosenUser != null) {
					
					dataChosenUser.setText("<html>Daten vom Benutzer <b>" + 
							Model.chosenUser + "</b></html>");
				}
					
				
				JScrollPane tablePane = new JScrollPane(table);
			
				frame.add(tablePane, "cell 0 0, span");
				
				if (!Model.ownData) {
					frame.add(chooseUserButton, "cell 1 1, align right");
					frame.add(chooseUserField, "cell 1 1");
					
					frame.add(deleteEntryButton, "cell 0 1");
				}
				
				frame.add(backButton, "cell 0 2");
				
				// frame.add(dataChosenUser, "cell 0 2, span");
				
				frame.add(infoMsg, "cell 0 3, span");
				
				
				chooseUserButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {						
						String chosenUserStr = chooseUserField.getText();
						if (!Model.users.containsKey(chosenUserStr)) {
							// Extension that should make the application more robust	
							infoMsg.setText("<html>Benutzer <b>" + chosenUserStr +
									"</b> existiert nicht.</html>");
						} else {
							Model.chosenUser = Model.users.get(chosenUserStr);
							if (Model.chosenUser.getEntries().size() > 0) {
								dataForModel = new Vector<>();
								notes = new ArrayList<>();
								fillDataForModel(Model.chosenUser, dataForModel, notes);
								tableModel.setDataVector(
										dataForModel, 
										title
								);
							}
							chooseUserButton.setVisible(false);
							chooseUserField.setVisible(false);
							infoMsg.setText("<html>Benutzer <b>" + chosenUserStr +
									"</b> ausgewählt.</html>");
						}
						
					}
				});
				
				deleteEntryButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						// remove the selected row
						int selectedRow = table.getSelectedRow();
						LocalDate dateOfEntryToRemove = LocalDate.parse(dataForModel.get(selectedRow).get(0), Utilities.dateFormatter);
						// Checker whether it relates to the current day
						if (!Model.chosenUser.isAtWork() || 
								!dateOfEntryToRemove.isEqual(LocalDate.now())) 
						{
							// Deletes automatically in dataForModel
							tableModel.removeRow(selectedRow);
							Model.chosenUser.removeEntryOnDate(dateOfEntryToRemove);
							// Entry in the notes should be deleted as well
							notes.remove(selectedRow);
						}
						else {
							infoMsg.setText("Heutiger Eintrag kann nicht gelöscht werden.");
						}						
					}
				});
				
				backButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {						
						frame.setVisible(false);
						Main.activeWindowObject = new StartWindow();
						
					}
				});
				
			}

		});
	}
	
	/**
	 * Helper function, fills in data needed for the table
	 * @param user
	 * @param dataForModel
	 * @param notes
	 */
	private void fillDataForModel(model.User user, Vector<Vector<String>> dataForModel, ArrayList<Boolean> notes) {
		for (model.Entry entry : user.getEntries().values()) {
			notes.add(entry.isNote());
			if (entry instanceof model.TimeEntry) {
				model.TimeEntry timeEntry = (model.TimeEntry) entry;
				String dateStr = timeEntry.getDate().format(Utilities.dateFormatter);
				String startStr = timeEntry.getStartTime().format(Utilities.timeFormatter);
				String endStr;
				try {
					 endStr = timeEntry.getEndTime().format(Utilities.timeFormatter);
				} catch (Exception e) {
					endStr = "null";
				}
				dataForModel.add(new Vector<String>(Arrays.asList(dateStr, 
					startStr,
					endStr
				)));
			} else {
				model.AbsenceEntry absEntry = (model.AbsenceEntry) entry;
				String dateStr = absEntry.getDate().format(Utilities.dateFormatter);
				String absReasonStr = absEntry.getAbsenceReason();
				dataForModel.add(new Vector<String>(Arrays.asList(dateStr,
						absReasonStr,
						absReasonStr
				)));
			}
		}
	}
}
