package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.toedter.calendar.JDateChooser;

import model.Model;
import model.User;
import net.miginfocom.swing.MigLayout;

/**
 * Window for late input of users' working time.
 */
public class LateInput {

	// Attributes that were moved so that we can use them in tests
	JTextField usernameField;
	JDateChooser datumPicker;
	JSpinner startTimeSpinner;
	JSpinner endTimeSpinner;
	JButton confirmButton;
	JButton backButton;
	
	JLabel errorMsg = new JLabel("");
	JFrame frame;

	public LateInput() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.LATE_INPUT;

				// Create Frame
				frame = new JFrame("Verstpätete Eingabe der Komm- und Gehzeit");

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				// Layout Window
				frame.setLayout(new MigLayout("align 50% 50%"));
				

				// Objects of the Frame
				JLabel username = new JLabel("Benutzername");
				JLabel date = new JLabel("Datum");
				JLabel start = new JLabel("Kommzeit");
				JLabel end = new JLabel("Gehzeit");
				usernameField = new JTextField(10);
				datumPicker = new JDateChooser(new Date());
				
				// Late input cannot be done for a date in the future
				// Extension that should make the application more robust	
				datumPicker.setMaxSelectableDate(new Date());
				datumPicker.setLocale(Locale.GERMANY);
		
		
				SpinnerDateModel startModel = new SpinnerDateModel();
				startModel.setCalendarField(Calendar.MINUTE);

				startTimeSpinner = new JSpinner(startModel);
				startTimeSpinner.setEditor(new JSpinner.DateEditor(startTimeSpinner, "HH:mm:ss"));
				
				SpinnerDateModel endModel = new SpinnerDateModel();
				endModel.setCalendarField(Calendar.MINUTE);
				
				endTimeSpinner = new JSpinner(endModel);
				endTimeSpinner.setEditor(new JSpinner.DateEditor(endTimeSpinner, "HH:mm:ss"));
				confirmButton = new JButton("Bestätigen");
				
				backButton = new JButton("Zurück");

				frame.add(username, "cell 0 0");
				
				frame.add(usernameField, "cell 0 1");
				
				frame.add(backButton, "cell 1 2");
				
				frame.add(date, "cell 1 0");
				
				frame.add(start, "cell 2 0");
				
				frame.add(end, "cell 3 0");
				
				frame.add(datumPicker, "cell 1 1");
				
				frame.add(startTimeSpinner, "cell 2 1");
				
				frame.add(endTimeSpinner, "cell 3 1");
				
				frame.add(confirmButton, "cell 2 2, span 2");
				
				frame.add(errorMsg, "cell 0 3, span");

				// Window Settings
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);
				
				// Event when clicking on "Bestaetigen" button
				confirmButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						
						errorMsg.setText("Benutzer existiert nicht oder Kommzeit bevor Gehzeit");
						
						User user;
						if ((user = Model.users.get(usernameField.getText())) != null) {
							
							Date tmp = (Date) startTimeSpinner.getValue();
							LocalTime startTime = LocalDateTime.ofInstant(tmp.toInstant(), 
									ZoneId.systemDefault()).toLocalTime();
							
							tmp = (Date) endTimeSpinner.getValue();
							LocalTime endTime = LocalDateTime.ofInstant(tmp.toInstant(), 
									ZoneId.systemDefault()).toLocalTime();
														
							LocalDate tmpLD = LocalDateTime.ofInstant(datumPicker.getDate().toInstant(), 
									ZoneId.systemDefault()).toLocalDate();
														
							// Overwrite the date if it already exists
							user.addEntryOverwrite(new model.TimeEntry(tmpLD, startTime, endTime));

							frame.setVisible(false);						
							Main.activeWindowObject = new UserAdminWindow();
						} 
						
					}
				});
				
				// Event when clicking on "Zurueck" button
				backButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new UserAdminWindow();

					}
				});
			}
		});
	}
}