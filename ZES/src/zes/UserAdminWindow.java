package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.Model;
import model.Role;
import model.User;
import net.miginfocom.swing.MigLayout;

/**
 * Window that allows the user to administer the users.
 */
public class UserAdminWindow {
	
	// Attributes that were moved so that we can use them in tests
	JButton addNewUser;
	JButton deleteUser;
	JButton modifyUserDataButton;
	JButton lateInputButton;
	JButton back;
	
	JFrame frame;
	
	public UserAdminWindow() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.USER_ADMIN;
				
				// create our frame as usual
				frame = new JFrame("Benutzeradministration");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				// set layout
				frame.setLayout(new MigLayout(
						"insets 5 5 20 20, align 50% 50%",	// Layout constraints
						"",									// Column constraints
						"[]40[][][]"));						// Row constraints
				
				
				// Objects of the frame
				addNewUser = new JButton("Neuen Benutzer hinzufügen");
				addNewUser.setPreferredSize(new Dimension(250, 30));
				
				deleteUser = new JButton("Benutzer löschen");
				deleteUser.setPreferredSize(new Dimension(250, 30));
				
				// Administrator functions
				lateInputButton = new JButton("<html><center>Verspätete Eingabe <br/>der Arbeitszeit<center></html>");
				lateInputButton.setPreferredSize(new Dimension(250, 40));
				
				modifyUserDataButton = new JButton("Benutzerdaten modifizieren");
				modifyUserDataButton.setPreferredSize(new Dimension(250, 40));
				
				back = new JButton("Zurück");
				back.setPreferredSize(new Dimension(250, 40));
				
				User activeUser = Model.activeUser;
				Role activeRolle = Model.activeUser.getRole();
				
				JLabel logedInInfo = new JLabel("<html>Benutzer <b>" + activeUser.getName() + 
												"</b> in der Rolle <b>" + activeRolle.getName() + "</b></html>");
				
						
												
				frame.add(addNewUser, "cell 1 1");
				
				frame.add(deleteUser, "cell 1 2");
				
				frame.add(modifyUserDataButton, "cell 2 3");
				frame.add(lateInputButton, "cell 2 4");
				
				frame.add(back, "cell 1 5");
				
				frame.add(logedInInfo, "cell 1 0, align right, span 2");
				
				

				// set the frame size and location, and make it visible
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);

				// Event when clicking on fa button
				addNewUser.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new AddUser();

					}
				});
				// Event when clicking on db button
				deleteUser.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new DeleteUser();

					}
				});
				
				// Event when clicking on modifyUserData button
				modifyUserDataButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Model.ownData = false;
						Main.activeWindowObject = new DataWindow();
						
					}
				});
				
				// Event when clicking on lateInput button
				lateInputButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new LateInput();

					}
				});
				
				// Event when clicking on logoff button
				back.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new StartWindow();

					}
				});

			}
		});
	}

}
