package zes;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.Model;
import model.Role;
import model.User;
import net.miginfocom.swing.MigLayout;

/**
 * Start window, shows functions that are available to the user. 
 * Available functions depend on the user's role.
 *  
 * Remark: 
 * Implementierung hat 2 default Benutzer:
 * 		(1) Benutzername: <b>mitarbeiter</b> mit Passwort: <b>mitarbeiter</b> in der Rolle Mitarbeiter
 * 		(2) Benutzername: <b>admin</b> mit Passwort: <b>admin</b> in der Rolle Admin
 */
public class StartWindow {
		
	// Attributes that were moved so that we can use them in tests
	JButton startEndButton;
	JButton fehlzeitButton;
	JButton dataButton;
	JButton statisticsButton;
	JButton userAdminButton;
	JButton systemCustomizationButton;
	JButton roleAdministrationButton;
	JButton logoffButton;
	
	JFrame frame;
	

	public StartWindow() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException ex) {
				}
				
				// Set active window
				Main.activeWindow = Windows.START_WINDOW;
				
				// create our frame as usual
				frame = new JFrame("Startfenster");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				// set layout
				frame.setLayout(new MigLayout(
						"insets 5 5 20 20, align 50% 50%"));
				
				
				// Objects of the frame
				BufferedImage logo;
				JLabel logoLabel = null;
				try {
					logo = ImageIO.read(getClass().getResource("resources/Logo.png"));
					logoLabel = new JLabel(new ImageIcon(logo.getScaledInstance(60, 60, Image.SCALE_SMOOTH)));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				
				if (!Model.activeUser.isAtWork()) {
					startEndButton = new JButton("Kommzeit dokumentieren");
				} else {
					startEndButton = new JButton("Gehzeit dokumentieren");
				}
				
				startEndButton.setPreferredSize(new Dimension(250, 30));
				
				fehlzeitButton = new JButton("Fehlzeitgrund eingeben");
				fehlzeitButton.setPreferredSize(new Dimension(250, 30));
				
				dataButton = new JButton("Meine Daten");
				dataButton.setPreferredSize(new Dimension(250, 30));
				
				statisticsButton = new JButton("Statistiken");
				statisticsButton.setPreferredSize(new Dimension(250, 30));
				
				// Administrator functions
				
				userAdminButton = new JButton("Benutzeradministration");
				userAdminButton.setPreferredSize(new Dimension(250, 30));
				
				systemCustomizationButton = new JButton("Systemanpassung");
				systemCustomizationButton.setPreferredSize(new Dimension(250, 30));
				
				roleAdministrationButton = 
				        new JButton("<html><center>Administration "
				                + "der <br/> Berechtigungen<center></html");
				roleAdministrationButton.setPreferredSize(new Dimension(250, 40));
				
				logoffButton = new JButton("Abmelden");
				logoffButton.setPreferredSize(new Dimension(250, 40));
				
				User activeUser = Model.activeUser;
				Role activeRolle = Model.activeUser.getRole();
				
				JLabel logedInInfo = new JLabel("<html>Benutzer <b>" + activeUser.getName() + 
												"</b> in der Rolle <b>" + activeRolle.getName() + "</b></html>");
				
				if (logoLabel != null)	
					frame.add(logoLabel, "cell 0 0, align left");
				
				if (activeRolle.startEndTimeDocumentation)
					frame.add(startEndButton, "cell 1 1");
								
				if (activeRolle.absenceReasonInput)
					frame.add(fehlzeitButton, "cell 2 1");
				
				if (activeRolle.accessData) {
					frame.add(dataButton, "cell 2 2");
					frame.add(statisticsButton, "cell 1 3");
				}
				
				if (activeRolle.userAdministration)
					frame.add(userAdminButton, "cell 2 3");		
				
				if (activeRolle.systemAdjustment)
					frame.add(systemCustomizationButton, "cell 2 4");
				
				if (activeRolle.permissionsAdministration)
					frame.add(roleAdministrationButton, "cell 2 5");
				
				frame.add(logoffButton, "cell 1 5");
				
				frame.add(logedInInfo, "cell 1 0, align right, span 2");
				
				

				// set the frame size and location, and make it visible
				frame.setPreferredSize(new Dimension(500, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				frame.setResizable(false);

				// Event when clicking on "Komm/Gehzeit dokumentieren" button
				startEndButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						
						frame.setVisible(false);
						Main.activeWindowObject = new StartEndTimeDocumented();

					}
				});
				// Event when clicking on fa button
				fehlzeitButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new FehlzeitgrundEingabe();

					}
				});
				
				dataButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Model.ownData = true;
						Main.activeWindowObject = new DataWindow();

					}
				});
				
				statisticsButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new Statistics();
					}
				});
				
				userAdminButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new UserAdminWindow();
					}
				});
				
				
				systemCustomizationButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new SystemCustomization();
					}
				});
				
				roleAdministrationButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Main.activeWindowObject = new RoleAdmin();
					}
				});
				
				// Event when clicking on logoff button
				logoffButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						frame.setVisible(false);
						Model.activeUser = null;
						Main.activeWindowObject = new Authentication();

					}
				});

			}
		});
	}
}