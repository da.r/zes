# <img src='./images/logo.png' width='1024'>
----------------------------------------

- [Specification](1-Spezifikation.pdf) (German)
- [Project Plan](2-Projektplan.pdf) (German)

This Java project contains an implementation of the specification of the Zeiterfassungssystem ZES.
Design was intentionally kept simple to motivate and incite less experienced members of the the team to contribute to the project.  

Project contains a few dummy users and roles:
- Roles
    - Mitarbeiter
    - Admin
    - Personalbereich
- Users (name, password, rolle)
    - mitarbeiter, mitarbeiter, Mitarbeiter
    - admin, admin, Admin
    - muellerh, muellerh, Mitarbeiter
    - webers, webers, Personalbereich

To store data the system uses Space Separated Values file _systemData.txt_

Tests are contained in _TestZES_ class. When executing tests with coverage, thread sleep time sometimes needs to be somewhat longer.

The project contains the following dependencies which can be found in _lib_ folder:
- jcalendar-1.4
- miglayout-4.0-swing


## Usage
To run the application one could compile it (e.g. in eclipse) or use the generated .jar file by calling in the `jar` folder:

```
java -jar ZES.jar
```
If the CI/CD pipeline deployment stage finished successfully, one can use the generated artifact `zes.jar` in the same way.

### Container
Alternatively one could use build an image using the provided Dockerfile and run the app in a container. In this case one might possibly need to call `xhost +` before the container is run in order to enable access to the X Server. At some point after the app container is not needed, one should enable the access control to the X server with `xhost -`.

```
docker image build -t zes:latest .
docker run -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix zes:latest
```

### Pictures
|![Figure:authentication.png](./images/authentication.png) | ![Figure:startWindow.png](./images/startWindow.png) |
| :---: | :---: |
| *Fig. 1. Authentication* | *Fig. 2. Start Window* |
